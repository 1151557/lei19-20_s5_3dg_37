** Diagramas de Componentes - SPRINT A **
=======================================

### 1. Diagrama de componentes (implementação do MDF)

![](Diagrama_de_componentes_implementacao_do_MDF.jpg)

### 2. Diagrama de componentes do sistema

![](Diagrama_de_componentes_do_sistema.jpg)
