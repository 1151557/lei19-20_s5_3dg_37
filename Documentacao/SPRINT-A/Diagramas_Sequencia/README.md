** Diagramas de Sequencia - SPRINT A **
=======================================

### 1. Criar Tipo de Máquina e associar operações

![](RF01_SD.jpg)

### 2. Criar Máquina, definindo o respetivo tipo de máquina

![](RF02_SD.jpg)

### 3. Criar Linha de Produção, associando as respetivas máquinas

![](RF03_SD.jpg)

### 4. Criar Operação

![](RF04_SD.jpg)

### 5. Criar Produto, definindo o plano de fabrico

![](RF05_SD.jpg)

### 6. Alterar operações de tipo de máquina

![](RF06_SD.jpg)

### 7. Alterar tipo de máquina de máquina

![](RF07_SD.jpg)

### 8. Consultar Tipo de Máquina

![](RF08_SD.jpg)

### 9. Consultar Operações de Tipo de Máquina

![](RF09_SD.jpg)

### 10. Consultar Máquina

![](RF10_SD.jpg)

### 11. Consultar Máquinas de Tipo de Máquina

![](RF11_SD.jpg)

### 12. Consultar Produto

![](RF12_SD.jpg)

### 13. Consultar Plano de Fabrico de Produto

![](RF13_SD.jpg)
