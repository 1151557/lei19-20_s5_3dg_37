** SPRINT-A **
===============================


## Diagramas Desenvolvidos ##


| | Tipo     |
|--------|--------------------|
| **1**  | [Diagramas Sequencia](Diagramas_Sequencia) |
| **2**  | [Diagramas Componente](Diagramas_Componentes) |
| **3**  | [Diagramas Conteúdo](Diagramas_Conteudo) |


## Diagrama de contexto de sistema

![](Diagrama_de_contexto_de_sistema.jpg)

## Vista de cenários do sistema

![](Vista_de_cenários_do_sistema.jpg)
