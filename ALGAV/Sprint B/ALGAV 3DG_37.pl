% Linhas

linhas([lA]).


% Maquinas


maquinas([ma]).



% Ferramentas


ferramentas([fa,fb,fc]).


% Maquinas que constituem as Linhas

tipos_maq_linha(lA,[ma]).
% ...


% Operações

tipo_operacoes([opt1,opt2,opt3]).

% operacoes/1 deve ser criado dinamicamente
operacoes([op1,op2,op3,op4,op5]).

%operacoes_atrib_maq depois deve ser criado dinamicamente
operacoes_atrib_maq(ma,[op1,op2,op3,op4,op5]).

% classif_operacoes/2 deve ser criado dinamicamente %%atomic_concat(op,NumOp,Resultado)
classif_operacoes(op1,opt1).
classif_operacoes(op2,opt2).
classif_operacoes(op3,opt1).
classif_operacoes(op4,opt2).
classif_operacoes(op5,opt3).

%classif_operacoes(op6,opt1).
%classif_operacoes(op7,opt2).
%classif_operacoes(op8,opt1).
%classif_operacoes(op9,opt2).
%classif_operacoes(op10,opt3).
% ...


% Afetação de tipos de operações a tipos de máquinas
% com ferramentas, tempos de setup e tempos de execucao)

operacao_maquina(opt1,ma,fa,5,60).
operacao_maquina(opt2,ma,fb,6,30).
operacao_maquina(opt3,ma,fc,8,40).
%...


% PRODUTOS

produtos([pA,pB,pC]).

operacoes_produto(pA,[opt1]).
operacoes_produto(pB,[opt2]).
operacoes_produto(pC,[opt3]).



% ENCOMENDAS

%Clientes

clientes([clA,clB]).


% prioridades dos clientes

prioridade_cliente(clA,1).
prioridade_cliente(clB,2).
% ...

% Encomendas do cliente,
% termos e(<produto>,<n.unidades>,<tempo_conclusao>)

encomenda(clA,[e(pA,1,100),e(pB,1,100)]).
encomenda(clB,[e(pA,1,110),e(pB,1,150),e(pC,1,300)]).
% ...


% Separar posteriormente em varios ficheiros


% permuta/2 gera permutações de listas
permuta([ ],[ ]).
permuta(L,[X|L1]):-apaga1(X,L,Li),permuta(Li,L1).

apaga1(X,[X|L],L).
apaga1(X,[Y|L],[Y|L1]):-apaga1(X,L,L1).

% permuta_tempo/3 faz uma permutação das operações atribuída a uma
% maquina e calcula tempo de ocupação incluindo trocas de ferramentas

permuta_tempo(M,LP,Tempo):- operacoes_atrib_maq(M,L), 
							permuta(L,LP), soma_tempos(semfer,M,LP,Tempo).


soma_tempos(_,_,[],0).
soma_tempos(Fer,M,[Op|LOp],Tempo):- classif_operacoes(Op,Opt),
	operacao_maquina(Opt,M,Fer1,Tsetup,Texec),
	soma_tempos(Fer1,M,LOp,Tempo1),
	((Fer1==Fer,!,Tempo is Texec+Tempo1);
			Tempo is Tsetup+Texec+Tempo1).

% g) Implemente o predicado melhor_escalonamento(M,Lm,Tm) que se for
% chamado com o 1º argumento instanciado com o nome da máquina
% coloca em Lm a sequência (permutação) das operações que corresponda
% ao menor tempo de ocupação (Tm) da máquina. 

% melhor escalonamento com findall, gera todas as solucoes e escolhe melhor

melhor_escalonamento(M,Lm,Tm):-
				get_time(Ti),
				findall(p(LP,Tempo),
				permuta_tempo(M,LP,Tempo), LL),
				melhor_permuta(LL,Lm,Tm),
				get_time(Tf),Tcomp is Tf-Ti,
				write('GERADO EM '),write(Tcomp),
				write(' SEGUNDOS'),nl.

melhor_permuta([p(LP,Tempo)],LP,Tempo):-!.
melhor_permuta([p(LP,Tempo)|LL],LPm,Tm):- melhor_permuta(LL,LP1,T1),
		((Tempo<T1,!,Tm is Tempo,LPm=LP);(Tm is T1,LPm=LP1)).


% i)  Já está no pdf de ajuda
:- dynamic melhor_sol_to/2.

melhor_escalonamento1(M,Lm,Tm):-get_time(Ti),
								(melhor_escalonamento11(M);true),retract(melhor_sol_to(Lm,Tm)),
								get_time(Tf),Tcomp is Tf-Ti,
								write('GERADO EM '),write(Tcomp),
								write(' SEGUNDOS'),nl.

melhor_escalonamento11(M):- asserta(melhor_sol_to(_,10000)),!,
							permuta_tempo(M,LP,Tempo),
							atualiza(LP,Tempo),
							fail.

atualiza(LP,T):-melhor_sol_to(_,Tm),
				T<Tm,retract(melhor_sol_to(_,_)),asserta(melhor_sol_to(LP,T)),!.


% k) Analise o tempo de processamento computacional para a geração da
% melhor solução através dos predicados
% melhor_escalonamento(M,Lm,Tm) e
% melhor_escalonamento1(M,Lm,Tm). Para o efeito use o predicado
% get_time/1. 


% l) Que heurística faria sentido usar se o único critério pretendido fosse o da
% minimização do tempo de ocupação (preparação mais processamento
% das operações na máquina). Escreva um predicado
% h_m_tempo_ocupacao(M,Lm,Tm) que aplique essa heurística. 

% Heurística de Setup (as tarefas com a mesma preparação são tratadas de seguinda), evitanto assim que a maquina precise de diversas preparações.

h_m_tempo_ocupacao(M,Lm,Tm):-operacoes_atrib_maq(M,[H|Lops]),h_m_tempo_ocupacao1([H|Lops],[],M,Tm,Lm),!.

h_m_tempo_ocupacao1([],Lt,_,0,Lm):-permuta(Lt,Lm),!.

h_m_tempo_ocupacao1([H|Lops],Lt,M,Tm,Lm):-classif_operacoes(H,Opt),operacao_maquina(Opt,M,_,Tp,Te),!,
					((classif_operacoes(X,Opt),member(X, Lops),delete(Lops,X,Nlops),h_m_tempo_ocupacao1([X|Nlops],[H|Lt],M,T,Lm), Tm is T + Te); h_m_tempo_ocupacao1(Lops,[H|Lt],M,T,Lm),Tm is T + Tp + Te).

%l com a*

h_m_tempo_ocupacao_astar(M,Cam,Custo):-operacoes_atrib_maq(M,Lops),aStarOcupacao(Lops,Cam,Custo).

aStarOcupacao(LOp,Cam,Custo):- aStarOcupacao2([(_,0,[],LOp)],Cam,Custo).

aStarOcupacao2([(_,Custo,LOp,[])|_],Cam,Custo):-!, reverse(LOp,Cam).

aStarOcupacao2([(_,Ca,LOp,LOpFaltam)|Outros],Cam,Custo):-
	findall((CEX,CaX,[X|LOp],Resto),
			(member(X,LOpFaltam),
				apaga1(X,LOpFaltam,Resto),
				op_prod_client(X,_,_,_,_,_,_,Tsetup,Texec),
				((verMesmaFer(X,LOp), ! ,(CaX is Ca + Texec));(CaX is Ca + Texec + Tsetup)),
				remMesmaFer(X,Resto,RestoX),
				estimativa(RestoX,Estimativa),
				CEX is CaX + Estimativa),
			Novos),
	append(Outros,Novos,Todos),
	sort(Todos,TodosOrd),
	aStarOcupacao2(TodosOrd,Cam,Custo).


verMesmaFer(X,[C|_]):-classif_operacoes(X,OPT1),classif_operacoes(C,OPT2), operacao_maquina(OPT2,_,F,_,_), operacao_maquina(OPT1,_,F,_,_).


remMesmaFer(_,[],[]) :- !.
remMesmaFer(X, [T|Xs], Y) :- !, classif_operacoes(T,OPT), ((operacao_maquina(OPT,_,X,_,_), ! , remMesmaFer(X, Xs, Y));(remMesmaFer(X, Xs, Y2)),append([T], Y2 , Y)).

estimativa(LOp,Estimativa):-
	findall(p(FOp,Tsetup),
	(member(Op,LOp),op_prod_client(Op,_,FOp,_,_,_,_,Tsetup,_)),
	LFTsetup),
	elimina_repetidos(LFTsetup,L),
	soma_setups(L,Estimativa).


elimina_repetidos([],[]).
elimina_repetidos([X|L],L1):-member(X,L),!,elimina_repetidos(L,L1).
elimina_repetidos([X|L],[X|L1]):-elimina_repetidos(L,L1).

soma_setups([],0).
soma_setups([p(_,Tsetup)|L],Ttotal):- soma_setups(L,T1), Ttotal is Tsetup+T1.


% m) ficha 5 cria_op_enc/0 - Cria factos (base de conhecimentos) relacionados com as opera��es a
% partir das encomendas
:- dynamic operacoes_atrib_maq/2.
:- dynamic classif_operacoes/2.
:- dynamic op_prod_client/9.
:- dynamic operacoes/1.


cria_op_enc:-retractall(operacoes(_)),
retractall(operacoes_atrib_maq(_,_)),retractall(classif_operacoes(_,_)),
retractall(op_prod_client(_,_,_,_,_,_,_,_,_)),
		findall(t(Cliente,Prod,Qt,TConc),
		(encomenda(Cliente,LE),member(e(Prod,Qt,TConc),LE)),
		LT),cria_ops(LT,0),
findall(Op,classif_operacoes(Op,_),LOp),asserta(operacoes(LOp)),
maquinas(LM),
 findall(_,
		(member(M,LM),
		 findall(Opx,op_prod_client(Opx,M,_,_,_,_,_,_,_),LOpx),
		 assertz(operacoes_atrib_maq(M,LOpx))),_).

cria_ops([],_).
cria_ops([t(Cliente,Prod,Qt,TConc)|LT],N):-
			operacoes_produto(Prod,LOpt),
	cria_ops_prod_cliente(LOpt,Cliente,Prod,Qt,TConc,N,N1),
			cria_ops(LT,N1).


cria_ops_prod_cliente([],_,_,_,_,Nf,Nf).
cria_ops_prod_cliente([Opt|LOpt],Client,Prod,Qt,TConc,N,Nf):-
		cria_ops_prod_cliente2(Opt,Prod,Client,Qt,TConc,N,Ni),
	cria_ops_prod_cliente(LOpt,Client,Prod,Qt,TConc,Ni,Nf).


cria_ops_prod_cliente2(_,_,_,0,_,Ni,Ni):-!.
cria_ops_prod_cliente2(Opt,Prod,Client,Qt,TConc,N,Ni2):-
			Ni is N+1,
			atomic_concat(op,Ni,Op),
			assertz(classif_operacoes(Op,Opt)),
			operacao_maquina(Opt,M,F,Tsetup,Texec),
	assertz(op_prod_client(Op,M,F,Prod,Client,Qt,TConc,Tsetup,Texec)),
	Qt2 is Qt -1,
	cria_ops_prod_cliente2(Opt,Prod,Client,Qt2,TConc,Ni,Ni2).




:-cria_op_enc.

% n) Considere agora o critério de minimização dos tempos de atraso na
% conclusão das operações e implemente o predicado
% melhor_escalonamento2(M,LAm,TAm) correspondente aos que realizou
% anteriormente na alínea j). 

:- dynamic melhor_sol_to/2.

melhor_escalonamento2(M,LAm,TAm):-get_time(Ti),
								(melhor_escalonamento22(M);true),retract(melhor_sol_to(LAm,TAm)),
								get_time(Tf),Tcomp is Tf-Ti,
								write('GERADO EM '),write(Tcomp),
								write(' SEGUNDOS'),nl.

melhor_escalonamento22(M):- asserta(melhor_sol_to(_,10000)),!,
							get_all_enc(M,Tempo,LP),
							atualiza(LP,Tempo),
							fail.

get_all_enc(M, Tempo, LP):-findall(e(TConc,Tsetup,Texec,Op,C,P,Q), op_prod_client(Op,M,_,P,C,Q,TConc,Tsetup,Texec), Lenc),
								  sort(Lenc, LOrdP), !, get_tempo_atraso(M, LOrdP, Tempo ,LP, [], semfer, 0),!.


get_tempo_atraso(_,[],0,L,LNops, _, _):-reverse(LNops, LOrd), permuta(LOrd,L).
get_tempo_atraso(M, [e(TConc,Tsetup,Texec,Op,C,P,Q)|Lenc],Tempo, L, Lops, UFer, TtotalExec):-op_prod_client(Op, M, Fer, P, C,_,TConc,Tsetup,Texec), !,
    																					TtotalExecAux is TtotalExec+Texec, ((Fer\==UFer,NTtotalExec is TtotalExecAux+Tsetup);NTtotalExec is TtotalExecAux),
    																					((TConc-NTtotalExec>=0,!,TAtraso is 0);(TAtraso is NTtotalExec-TConc)),
    																					get_tempo_atraso(M,Lenc,NTempo,L,[e(C,P,Q,TConc)|Lops],Fer,NTtotalExec),!,
    																					Tempo is NTempo + TAtraso.