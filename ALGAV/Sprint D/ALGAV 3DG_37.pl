% F�BRICA

% Linhas

linhas([lA,lB,lC]).


% Maquinas

% as m�quinas mf at� mj s�o iguais �s m�quinas ma at� me e constituem a linha lB igual a lA
% as m�quinas mk at� mm constituem a linha lC
maquinas([ma,mb,mc,md,me,mf,mg,mh,mi,mj,mk,ml,mm]).



% Ferramentas

% as ferramentas fa1 at� fj1 s�o iguais �s ferramentas fa a fj, sendo usadas pelas m�quinas mf at� mj

ferramentas([fa,fb,fc,fd,fe,ff,fg,fh,fi,fj,fa1,fb1,fc1,fd1,fe1,ff1,fg1,fh1,fi1,fj1,fk,fl,fm]).


% Maquinas que constituem as Linhas

tipos_maq_linha(lA,[ma,mb,mc,md,me]).
tipos_maq_linha(lB,[mf,mg,mh,mi,mj]).
tipos_maq_linha(lC,[mk,ml,mm]).


% ...


% Opera��es

tipo_operacoes([opt1,opt2,opt3,opt4,opt5,opt6,opt7,opt8,opt9,opt10,opt11,opt12,opt13]).

% operacoes/1 vai ser criado dinamicamente
%no exemplo dara' uma lista com 92 opera��es: 50 operacoes pelos 10 lotes de produtos * 5 operacoes que poder�o ir para as linhas lA ou lB mais 14 lotes de produtos * 3 opera��es que ir�o para a linha lC

%operacoes_atrib_maq/2 vai ser criado dinamicamente
%no exemplo as m�quinas mk a mm ter�o 14 operacoes atribuidas, uma por cada lote de produtos que ir� para lC; as opera��es atribu�das �s m�quinas ma at� mj dependem do balanceamento que for feito

% classif_operacoes/2 deve ser criado dinamicamente
%no exemplo teremos 92 factos deste tipo, um para cada operacao


% Afeta��o de tipos de opera��es a tipos de m�quinas
% com ferramentas, tempos de setup e tempos de execucao)

operacao_maquina(opt1,ma,fa,1,1).
operacao_maquina(opt2,mb,fb,2.5,2).
operacao_maquina(opt3,mc,fc,1,3).
operacao_maquina(opt4,md,fd,1,1).
operacao_maquina(opt5,me,fe,2,3).
operacao_maquina(opt6,mb,ff,1,4).
operacao_maquina(opt7,md,fg,2,5).
operacao_maquina(opt8,ma,fh,1,6).
operacao_maquina(opt9,me,fi,1,7).
operacao_maquina(opt10,mc,fj,20,2).
operacao_maquina(opt1,mf,fa1,1,1).
operacao_maquina(opt2,mg,fb1,2.5,2).
operacao_maquina(opt3,mh,fc1,1,3).
operacao_maquina(opt4,mi,fd1,1,1).
operacao_maquina(opt5,mj,fe1,2,3).
operacao_maquina(opt6,mg,ff1,1,4).
operacao_maquina(opt7,mi,fg1,2,5).
operacao_maquina(opt8,mf,fh1,1,6).
operacao_maquina(opt9,mj,fi1,1,7).
operacao_maquina(opt10,mh,fj1,20,2).
operacao_maquina(opt11,mk,fk,3,2).
operacao_maquina(opt12,ml,fl,1,4).
operacao_maquina(opt13,mm,fm,1,3).






%...


% PRODUTOS

% os produtos pA at� pF podem ser fabricados em lA ou lB
% os produtos pG at� pJ s� podem ser fabricados em lC

produtos([pA,pB,pC,pD,pE,pF,pG,pH,pI,pJ]).

operacoes_produto(pA,[opt1,opt2,opt3,opt4,opt5]).
operacoes_produto(pB,[opt1,opt6,opt3,opt4,opt5]).
operacoes_produto(pC,[opt1,opt2,opt3,opt7,opt5]).
operacoes_produto(pD,[opt8,opt2,opt3,opt4,opt5]).
operacoes_produto(pE,[opt1,opt2,opt3,opt4,opt9]).
operacoes_produto(pF,[opt1,opt2,opt10,opt4,opt5]).
operacoes_produto(pG,[opt11,opt12,opt13]).
operacoes_produto(pH,[opt11,opt12,opt13]).
operacoes_produto(pI,[opt11,opt12,opt13]).
operacoes_produto(pJ,[opt11,opt12,opt13]).



% ENCOMENDAS

%Clientes

clientes([clA,clB,clC,clD,clE,clF,clG]).


% prioridades dos clientes

prioridade_cliente(clA,2).
prioridade_cliente(clB,1).
prioridade_cliente(clC,3).
prioridade_cliente(clD,1).
prioridade_cliente(clE,1).
prioridade_cliente(clF,1).
prioridade_cliente(clG,1).

% ...

% Encomendas do cliente,
% termos e(<produto>,<n.unidades>,<tempo_conclusao>)

% encomendas que poderao ir para a linha lA ou lB
encomenda(clA,[e(pA,4,50)]).
encomenda(clA,[e(pB,4,70)]).
encomenda(clB,[e(pC,3,30)]).
encomenda(clB,[e(pD,5,200)]).
encomenda(clC,[e(pE,4,60)]).
encomenda(clC,[e(pF,6,120)]).
encomenda(clA,[e(pD,1,500)]).
encomenda(clA,[e(pF,20,450)]).
encomenda(clB,[e(pB,4,100)]).
encomenda(clC,[e(pA,3,100)]).

% encomendas que ir  para a linha lC
encomenda(clD,[e(pG,5,40)]).
encomenda(clE,[e(pH,3,30)]).
encomenda(clF,[e(pI,4,60)]).
encomenda(clG,[e(pJ,5,140)]).
encomenda(clD,[e(pJ,5,200)]).
encomenda(clE,[e(pG,4,150)]).
ncomenda(clF,[e(pH,3,180)]).
encomenda(clG,[e(pI,2,100)]).
encomenda(clA,[e(pI,4,100)]).
encomenda(clA,[e(pH,5,170)]).
encomenda(clB,[e(pG,5,230)]).
encomenda(clB,[e(pJ,3,250)]).
encomenda(clC,[e(pG,6,280)]).
encomenda(clC,[e(pH,6,300)]).


% ...
%
% cria_op_enc - fizeram-se correcoes face a versao anterior

:- dynamic operacoes_atrib_maq/2.
:- dynamic classif_operacoes/2.
:- dynamic op_prod_client/9.
:- dynamic operacoes/1.


cria_op_enc:-retractall(operacoes(_)),
	     retractall(operacoes_atrib_maq(_,_)),retractall(classif_operacoes(_,_)),
	     retractall(op_prod_client(_,_,_,_,_,_,_,_,_)),
	     findall(t(Cliente,Prod,Qt,TConc),
	     (encomenda(Cliente,LE),member(e(Prod,Qt,TConc),LE)),
	     LT),cria_ops(LT,0),
	     findall(Op,classif_operacoes(Op,_),LOp),asserta(operacoes(LOp)),
	     maquinas(LM),
	     findall(_,
		(member(M,LM),
		 findall(Opx,op_prod_client(Opx,M,_,_,_,_,_,_,_),LOpx),
		 assertz(operacoes_atrib_maq(M,LOpx))),_).

cria_ops([],_).
cria_ops([t(Cliente,Prod,Qt,TConc)|LT],N):-
			operacoes_produto(Prod,LOpt),
	cria_ops_prod_cliente(LOpt,Cliente,Prod,Qt,TConc,N,N1),
			cria_ops(LT,N1).


cria_ops_prod_cliente([],_,_,_,_,Nf,Nf).
cria_ops_prod_cliente([Opt|LOpt],Client,Prod,Qt,TConc,N,Nf):-
		cria_ops_prod_cliente2(Opt,Prod,Client,Qt,TConc,N,Ni),
	cria_ops_prod_cliente(LOpt,Client,Prod,Qt,TConc,Ni,Nf).


cria_ops_prod_cliente2(Opt,Prod,Client,Qt,TConc,N,Ni):-
			Ni is N+1,
			atomic_concat(op,Ni,Op),
			assertz(classif_operacoes(Op,Opt)),
			operacao_maquina(Opt,M,F,Tsetup,Texec),
	assertz(op_prod_client(Op,M,F,Prod,Client,Qt,TConc,Tsetup,Texec)).


:-cria_op_enc.

% ---------------------------------MAKESPAN---------------------------------

:- dynamic tarefa/4.



getAllCliPrioEnc(LCliEncs):-
			findall((C,P,Lenc),(prioridade_cliente(C,P),
			encomenda(C, Lenc)),LCliEncs),
			retractall(tarefa(_,_,_,_)), constroi(LCliEncs).

constroi([]).
constroi([H|L]):-calcular2((H),0),constroi(L).

calcular2((_,_,[]),_).
calcular2((Cliente, Prioridade, [e(Prod,NUnidades,TConc)|Le]), ID):-
		operacoes_produto(Prod,ListaTipos),
                operacoes_do_produto(ListaTipos,Lops),
                maior_tempo2(Lops,TempoM),
                calcular1(ListaTipos,Tempo),
                tempos(Lops,Menor),
		makespan(NUnidades,TempoM,Tempo,Menor,Makespan),
		string_concat(Cliente, "_", Cli),
		string_concat(Cli, ID, Nid),
		criar_tarefa(Nid,Makespan,TConc,Prioridade),!,
		IncId is ID + 1,
		calcular2((Cliente,Prioridade,Le),IncId),!.


makespan(Nlotes,Tmaior,Somatorio,Menor,Total):-
			SomTMenor is Somatorio-Tmaior,
			Total is Nlotes*Tmaior+SomTMenor-Menor, !,
			write(Nlotes),write('*'), write(Tmaior), write('+'),
			write(SomTMenor),write('-('),write(Menor), write(')='),write(Total), nl.

criar_tarefa(Id,Makespan,TConc,P):-assertz(tarefa(Id,Makespan,TConc,P)).


calcular1([],0).
calcular1([T|ListaTipos],Tempo):- operacao_maquina(T,_,_,_,Texec),!,
			calcular1(ListaTipos,Te),Tempo is Te+Texec.

operacoes_do_produto([],_).
operacoes_do_produto([T|ListTipo],[O|Lops]):-classif_operacoes(O,T),
			operacoes_do_produto(ListTipo,Lops).

maior_tempo2([],0).
maior_tempo2([Op|Lops],Tempo):-classif_operacoes(Op,Tipo),
			operacao_maquina(Tipo,_,_,_,TempoExec),
			maior_tempo2(Lops,TempoM),!,((TempoM<TempoExec,
			Tempo is TempoExec);Tempo is TempoM).

%a contar com os tempos de setup


calcula_tempos([],_,_).
calcula_tempos([Op|Lops],[T|Tempo],TempoExe):-
			classif_operacoes(Op,Tipo),
			operacao_maquina(Tipo,_,_,TempoSetup,TempoE),
			TempoExe1 is TempoExe+TempoE,
			T is TempoExe-TempoSetup,
			calcula_tempos(Lops,Tempo,TempoExe1).

menor_tempo_s([],0).
menor_tempo_s([T|Tempo], Menor):-calcula_tempos(_,[T|Tempo],0),
			menor_tempo_s(Tempo,Tm),!,
			((T<Tm, Menor is T);Menor is Tm).

tempos([],0).
tempos(Lops,Menor):-calcula_tempos(Lops,Tempo,0),
	      menor_tempo_s(Tempo,Menor).
		  
		  
%------ALGORITMO GENÉTICO-------------

:-dynamic geracoes/1.
:-dynamic populacao/1.
:-dynamic prob_cruzamento/1.
:-dynamic prob_mutacao/1.


% tarefa(Id,TempoProcessamento,TempConc,PesoPenalizacao).
%tarefa(t1,2,5,1).
%tarefa(t2,4,7,6).
%tarefa(t3,1,11,2).
%tarefa(t4,3,9,3).
%tarefa(t5,3,8,2).

% tarefas(NTarefas).
tarefas(6).


inicializa:-write('Numero de novas Geracoes: '),read(NG),
        (retract(geracoes(_));true), asserta(geracoes(NG)),
	write('Dimensao da Populacao: '),read(DP),
	(retract(populacao(_));true), asserta(populacao(DP)),
	write('Probabilidade de Cruzamento (%):'), read(P1),
	PC is P1/100,
	(retract(prob_cruzamento(_));true),
	asserta(prob_cruzamento(PC)),
	write('Probabilidade de Mutacao (%):'), read(P2),
	PM is P2/100,
	(retract(prob_mutacao(_));true), asserta(prob_mutacao(PM)).


gera:-	inicializa,
	gera_populacao(Pop),
	write('Pop='),write(Pop),nl,
	avalia_populacao(Pop,PopAv),
	write('PopAv='),write(PopAv),nl,
	ordena_populacao(PopAv,PopOrd),
	geracoes(NG),
	gera_geracao1(0,NG,PopOrd).

gera_populacao(Pop):-
	populacao(TamPop),
	tarefas(NumT),
	findall(Tarefa,tarefa(Tarefa,_,_,_),ListaTarefas),
	gera_populacao(TamPop,TamPop,ListaTarefas,NumT,Pop).

gera_populacao(0,_,_,_,[]):-!.

gera_populacao(TamPop,TamPop2,ListaTarefas,NumT,[Ind|Resto]):-
	TamPop1 is TamPop-1,
	gera_populacao(TamPop1,TamPop2,ListaTarefas,NumT,Resto),
	TamPop3 is TamPop2-1,TamPop4 is TamPop2-2,
	(((TamPop1==TamPop3,ordena_tarefa_edd(_,Ind));
	TamPop1==TamPop4,ordena_tarefa_folga(_,Ind));
	gera_individuo(ListaTarefas,NumT,Ind)),
	not(member(Ind,Resto)).

gera_populacao(TamPop,TamPop2,ListaTarefas,NumT,L):-
	gera_populacao(TamPop,TamPop2,ListaTarefas,NumT,L).

gera_individuo([G],1,[G]):-!.

gera_individuo(ListaTarefas,NumT,[G|Resto]):-
	NumTemp is NumT + 1, % To use with random
	random(1,NumTemp,N),
	retira(N,ListaTarefas,G,NovaLista),
	NumT1 is NumT-1,
	gera_individuo(NovaLista,NumT1,Resto).



retira(1,[G|Resto],G,Resto).
retira(N,[G1|Resto],G,[G1|Resto1]):-
	N1 is N-1,
	retira(N1,Resto,G,Resto1).

avalia_populacao([],[]).
avalia_populacao([Ind|Resto],[Ind*V|Resto1]):-
	avalia(Ind,V),
	avalia_populacao(Resto,Resto1).



avalia(Seq,V):-
	avalia(Seq,0,V).

avalia([],_,0).
avalia([T|Resto],Inst,V):-
	tarefa(T,Dur,Prazo,Pen),
	InstFim is Inst+Dur,
	avalia(Resto,InstFim,VResto),
	((InstFim =< Prazo,!, VT is 0);
	(VT is (InstFim-Prazo)*Pen)),
	V is VT+VResto.

ordena_populacao(PopAv,PopAvOrd):-
	bsort(PopAv,PopAvOrd).

bsort([X],[X]):-!.
bsort([X|Xs],Ys):-
	bsort(Xs,Zs),
	btroca([X|Zs],Ys).


btroca([X],[X]):-!.

btroca([X*VX,Y*VY|L1],[Y*VY|L2]):-
	VX>VY,!,
	btroca([X*VX|L1],L2).

btroca([X|L1],[X|L2]):-btroca(L1,L2).


ordena_tarefa_edd(ListaOrd,I):-findall(t(Tconc,Makespan,Prio,Id),
				    tarefa(Id,Makespan,Tconc,Prio),
				    Lista), sort(Lista,ListaOrd),
				    converter_edd(ListaOrd,I),
				    write(I).



ordena_tarefa_folga(LOrd,I):-findall(t(F,Prio,Id),
				 (tarefa(Id,Makespan,Tconc,Prio),
				  F is Tconc-Makespan),
				  Lista),
				  sort(Lista,LOrd),
				  converter_tarefa_folga(LOrd,I).

converter_edd([],[]).
converter_edd([t(_,_,_,Id)|ListaOrd],[Id|Individuo]):-
		 converter_edd(ListaOrd,Individuo).

converter_tarefa_folga([],[]).
converter_tarefa_folga([t(_,_,ID)|ListaOrd],[ID|Individuo]):-
			converter_tarefa_folga(ListaOrd,Individuo).



gera_geracao(G,G,Pop):-!,
	write('Geracao '), write(G), write(':'), nl, write(Pop), nl.

gera_geracao(N,G,Pop):-
	write('Geracao '), write(N), write(':'), nl, write(Pop), nl,
	random_permutation(Pop,LRP),
	cruzamento(LRP,NPop1),
	mutacao(NPop1,NPop),
	avalia_populacao(NPop,NPopAv),
	ordena_populacao(NPopAv,NPopOrd),
	N1 is N+1,
	gera_geracao(N1,G,NPopOrd).

gera_geracao1(G,G,Pop):-!,
	write('Geracao '), write(G), write(':'), nl, write(Pop), nl.

gera_geracao1(N,G,Pop):-
	write('Geracao '), write(N), write(':'), nl, write(Pop), nl,
	random_permutation(Pop,LRP),
	cruzamento(LRP,NPop1),
	mutacao(NPop1,NPop),
	avalia_populacao(NPop,NPopAv),
	append(Pop,NPopAv,PopTot),
	sort(PopTot,PopTotSemRepetidos),
	ordena_populacao(PopTotSemRepetidos,NPopTot),!,
	escolher_melhor_individuos(NPopTot,NPopOrd),
	N1 is N+1,
	gera_geracao1(N1,G,NPopOrd).


escolher_melhor_individuos([Ind1,Ind2|Resto],NovaPop):-
			Melhores=[Ind1,Ind2],
			torneios(Resto,Sobreviventes),
		        append(Melhores,Sobreviventes,NovaPop).

torneios(Resto,Sobreviventes):-populacao(A),
			DimSob is A-2,
			length(Resto,DimRest),
			((DimSob==DimRest,Sobreviventes=Resto);
			random_permutation(Resto,NResto),
			 torneio(NResto,DimSob,Sobreviventes)).

torneio([Ind1,Ind2|Resto],DimSob,Sobreviventes):-
			combate(Ind1,Ind2,Vencedor),
			append([Vencedor],Resto,NLista),
			length(NLista,DimLista),
			((DimLista==DimSob,Sobreviventes=NLista);
			random_permutation(NLista,NListaPerm),
			torneio(NListaPerm,DimSob,Sobreviventes)).


combate(Ind1,Ind2,Vencedor):-
			remover_valor(Ind1,V1),
			remover_valor(Ind2,V2),
			random(0,V1,ValorCombate1),
			random(0,V2,ValorCombate2),
			Total is V1+V2,
			ResultadoComb1 is ValorCombate1/Total,
			ResultadoComb2 is ValorCombate2/Total,
			((ResultadoComb1<ResultadoComb2,Vencedor=Ind1);
			(ResultadoComb1>ResultadoComb2,Vencedor=Ind2);
			Vencedor=Ind1).

remover_valor(_*V,V).

gerar_pontos_cruzamento(P1,P2):-
	gerar_pontos_cruzamento1(P1,P2).

gerar_pontos_cruzamento1(P1,P2):-
	tarefas(N),
	NTemp is N+1,
	random(1,NTemp,P11),
	random(1,NTemp,P21),
	P11\==P21,!,
	((P11<P21,!,P1=P11,P2=P21);(P1=P21,P2=P11)).
gerar_pontos_cruzamento1(P1,P2):-
	gerar_pontos_cruzamento1(P1,P2).


cruzamento([],[]).
cruzamento([Ind*_],[Ind]).
cruzamento([Ind1*_,Ind2*_|Resto],[NInd1,NInd2|Resto1]):-
	gerar_pontos_cruzamento(P1,P2),
	write(    'p1:'),write(P1),write('    p2:'),write(P2), nl,
	prob_cruzamento(Pcruz),random(0.0,1.0,Pc),
	((Pc =< Pcruz,!,
        cruzar(Ind1,Ind2,P1,P2,NInd1),
	  cruzar(Ind2,Ind1,P1,P2,NInd2))
	;
	(NInd1=Ind1,NInd2=Ind2)),
	cruzamento(Resto,Resto1).

preencheh([],[]).

preencheh([_|R1],[h|R2]):-
	preencheh(R1,R2).


sublista(L1,I1,I2,L):-
	I1 < I2,!,
	sublista1(L1,I1,I2,L).

sublista(L1,I1,I2,L):-
	sublista1(L1,I2,I1,L).

sublista1([X|R1],1,1,[X|H]):-!,
	preencheh(R1,H).

sublista1([X|R1],1,N2,[X|R2]):-!,
	N3 is N2 - 1,
	sublista1(R1,1,N3,R2).

sublista1([_|R1],N1,N2,[h|R2]):-
	N3 is N1 - 1,
	N4 is N2 - 1,
	sublista1(R1,N3,N4,R2).

rotate_right(L,K,L1):-
	tarefas(N),
	T is N - K,
	rr(T,L,L1).

rr(0,L,L):-!.

rr(N,[X|R],R2):-
	N1 is N - 1,
	append(R,[X],R1),
	rr(N1,R1,R2).


elimina([],_,[]):-!.

elimina([X|R1],L,[X|R2]):-
	not(member(X,L)),!,
	elimina(R1,L,R2).

elimina([_|R1],L,R2):-
	elimina(R1,L,R2).

insere([],L,_,L):-!.
insere([X|R],L,N,L2):-
	tarefas(T),
	((N>T,!,N1 is N mod T);N1 = N),
	insere1(X,N1,L,L1),
	N2 is N + 1,
	insere(R,L1,N2,L2).


insere1(X,1,L,[X|L]):-!.
insere1(X,N,[Y|L],[Y|L1]):-
	N1 is N-1,
	insere1(X,N1,L,L1).

cruzar(Ind1,Ind2,P1,P2,NInd11):-
	sublista(Ind1,P1,P2,Sub1),
	tarefas(NumT),
	R is NumT-P2,
	rotate_right(Ind2,R,Ind21),
	elimina(Ind21,Sub1,Sub2),
	P3 is P2 + 1,
	insere(Sub2,Sub1,P3,NInd1),
	eliminah(NInd1,NInd11).


eliminah([],[]).

eliminah([h|R1],R2):-!,
	eliminah(R1,R2).

eliminah([X|R1],[X|R2]):-
	eliminah(R1,R2).

mutacao([],[]).
mutacao([Ind|Rest],[NInd|Rest1]):-
	prob_mutacao(Pmut),
	random(0.0,1.0,Pm),
	((Pm < Pmut,!,mutacao1(Ind,NInd));NInd = Ind),
	mutacao(Rest,Rest1).

mutacao1(Ind,NInd):-
	gerar_pontos_cruzamento(P1,P2),
	mutacao22(Ind,P1,P2,NInd).

mutacao22([G1|Ind],1,P2,[G2|NInd]):-
	!, P21 is P2-1,
	mutacao23(G1,P21,Ind,G2,NInd).
mutacao22([G|Ind],P1,P2,[G|NInd]):-
	P11 is P1-1, P21 is P2-1,
	mutacao22(Ind,P11,P21,NInd).

mutacao23(G1,1,[G2|Ind],G2,[G1|Ind]):-!.
mutacao23(G1,P,[G|Ind],G2,[G|NInd]):-
	P1 is P-1,
	mutacao23(G1,P1,Ind,G2,NInd).

%------ requisito 1 (Sprint D) --------
%------ balanceamento de linhas idênticas -----------

% para ordenar as encomendas por tempo de conclusão com o makespan
% incluído

ordena_encomendas_edd2(ListOrdE,L):-
            findall((e(Tconc,Qt,Prod,Makespan,Id)),
             (encomenda(Clie,_),
              tarefa(Id,Makespan,Tconc,_),
              split_string(Id, "_", "", [Cliente|_]),
              string_concat("",Clie,T),
              T == Cliente, encomenda(Clie,[e(Prod,Qt,Tconc)|_])),L),
              sort(L,ListOrdE).


%para chamar os predicados com as encomendas e linhas
percorre:-retractall(encomenda_l(_,_)),
      ordena_encomendas_edd2(Lenc,_),
      percorre_encomendas(Lenc).


%percorrer as encomendas e ver qual linha tem menor somatorio
percorre_encomendas([]).
percorre_encomendas([e(TConc,Qt,Prod,Makespan,Id)|Lenc]):-
            linhas_possiveis_produto(Prod,LL),
            nl, write("____________________________"),
            nl, write("Produto: "), write(Prod),
            nl, write("Linhas ACEITAVEIS: "), write(LL),nl,nl,nl,
            menor_somatorio(LL,_,Li),
            adiciona_enc(e(TConc,Qt,Prod,Makespan,Id),Li),
            percorre_encomendas(Lenc).


%ver o somatorio da linha de produção
calcula_somatorio([],0,_).
calcula_somatorio([e(_,_,_,Makespan,_)|Lenc],Som,Linha):-
             calcula_somatorio(Lenc,Somatorio,Linha),
         Som is Somatorio+Makespan.


%ver linha com menor somatorio das linhas de produção
menor_somatorio([],2000,_).
menor_somatorio([Linha|Linhas],Menor,Li):-
            encomendas_de_linha(Linha,Encomendas),
            calcula_somatorio(Encomendas,Som,Linha),
            write('Linha: '),write(Linha),nl,
            write('Somatório da linha:'),write(Som),nl,!,
            menor_somatorio(Linhas,Men,L),
               ((Men>=Som,Menor is Som,Li=Linha,!);
                Menor is Men,Li=L).

%adicionar encomenda à linha
adiciona_enc(Encomenda,Linha):-
          assertz(encomenda_l(Linha,Encomenda)).

%encomendas de uma linha de produção
encomendas_de_linha(Linha, Encomendas):-
                findall(Encomenda,
            encomenda_l(Linha,Encomenda)
                ,Encomendas).


%lista linhas com a sequencia de maquina que nos introduzirmos
%listar_maquinas_lista(Maquinas,L):-
%        findall(Linha,tipos_maq_linha(Linha,Maquinas),L).


listar_maquinas_lista(ListaOpTipo,L):-
         findall(Linha,
            (operacao_maquina(Opt,Maq,_,_,_), member(Opt,ListaOpTipo), tipos_maq_linha(Li,Maquinas), member(Maq,Maquinas), Linha = Li),L).


%listar maquinas que podem fazer aqueles tipos de operações
sequencia_maquinas([],_).
sequencia_maquinas([Opt|ListaOpTipo],[Maq|Maquinas]):-
            operacao_maquina(Opt,Maq,_,_,_),
            sequencia_maquinas(ListaOpTipo,Maquinas).

%linhas que podem produzir o produto
linhas_possiveis_produto(Prod,NLinhas):-
                  operacoes_produto(Prod,ListaOpTipo),
                 %sequencia_maquinas(ListaOpTipo,Maquinas),
                 listar_maquinas_lista(ListaOpTipo,Linhas),
                 sort(Linhas, NLinhas).

:-dynamic encomenda_l/2.

%------ requisito 2 (Sprint D) --------
%------ agendas temporais --------------

agenda_cliente(Cliente):-
         retractall(agenda_maq(_,_)),
         encomendas_cli(Cliente,Encomendas),
         write(Encomendas), nl,
	operacoes_tipo_encomendas(Encomendas,Cliente,1),!.

encomendas_cli(Cliente,Enc):-
    findall(Encomendas,encomenda(Cliente,Encomendas),Enc).


operacoes_tipo_encomendas([],_,_).
operacoes_tipo_encomendas([[e(Prod,Qt,_)]|Encomendas],Cliente,N):-
			operacoes_produto(Prod,ListaOpTipo),
			calcular_maquina_controlo(ListaOpTipo,0,MaqControlo,Tempo),
			calcular_tempo_acumulado(ListaOpTipo,TempoAcumulado),
			lista_tipo_operacoes(Prod,ListaOpTipo,Qt,TempoAcumulado,0,MaqControlo,Tempo,0,Cliente,N),
			N1 is N + 1,
			(   (N>1, retirar_setup_repetido(ListaOpTipo),
			menor_tempo_desl(ListaOpTipo,N,MenorTempo),
			deslizamento(ListaOpTipo,N,MenorTempo),operacoes_tipo_encomendas(Encomendas,Cliente,N1));
			operacoes_tipo_encomendas(Encomendas,Cliente,N1)).


lista_tipo_operacoes(_,[],_,_,_,_,_,_,_,_).
lista_tipo_operacoes(Prod,[Tipo|ListaOpTipo],Qt,TempoAcumulado,AcumuladoMaqSp,MaqControlo,TempoControlo,Espera,Cliente,N):-
	    operacao_maquina(Tipo,Maquina,Ferramenta,Setup,Exec),
	    junta(Prod, Tipo,Maquina,Ferramenta,Setup,Exec,Qt,MaqControlo,TempoControlo,
		  TempoAcumulado,AcumuladoMaqSp,Espera,Cliente,N),
	    Acumulado is AcumuladoMaqSp + Exec,
	    (	(Espera>Exec, Espera1 = Espera);
	    Espera1 = Exec),
	    lista_tipo_operacoes(Prod,ListaOpTipo,Qt,TempoAcumulado,Acumulado,MaqControlo,TempoControlo,Espera1,Cliente,N).


:-dynamic agenda_maq/2.


junta(Prod, Tipo,Maquina,Ferramenta,Setup,Exec,Qt,MaqControlo,TempoControlo,TempoAcumulado,AcumuladoMaqSp,Espera,Cliente,N):-
			(   (Maquina == MaqControlo, TimeInicioSet = TempoAcumulado, TimeInicioExec is TempoAcumulado + Setup);
			(   TI is - TempoControlo,
			TimeInicioExec is TempoAcumulado + AcumuladoMaqSp + TI,
			TimeInicioSet is TimeInicioExec - Setup)),
			FimSet = TimeInicioExec,
			(   (Espera>Exec, TE = Espera, Qt2 is Qt-1 ,Dur is TE *Qt2,
			     Duracao is Dur + Exec);
			(Duracao = Exec * Qt)),
			FimExec is TimeInicioExec + Duracao,
			number_string(N,Num),
			string_concat(t,Num,T),
			(   (agenda_maq(Maquina,A), append(A,[t(TimeInicioSet,FimSet,'setup',Ferramenta),
					 t(TimeInicioExec,FimExec,'exec',[Tipo,Qt,Prod,Cliente,T])],B),
			     retract(agenda_maq(Maquina,A)), assertz(agenda_maq(Maquina,B)));
			(   assertz(agenda_maq(Maquina,[t(TimeInicioSet,FimSet,'setup',Ferramenta),
						    t(TimeInicioExec,FimExec,'exec',[Tipo,Qt,Prod,Cliente,T])])))),!.


calcular_maquina_controlo([],_,_,1000).

calcular_maquina_controlo([Tipo|Lista],N,MaqControlo,TempoControlo):-operacao_maquina(Tipo,Maq,_,Setup,Exec),
			N2 is N+Exec,
			calcular_maquina_controlo(Lista,N2,Maq2,Tempo2),
			Cand is N - Setup,
			(   (Cand<Tempo2, TempoControlo = Cand, MaqControlo = Maq);
			(   TempoControlo = Tempo2, MaqControlo = Maq2)),!.

calcular_tempo_acumulado([],0).
calcular_tempo_acumulado([Tipo|Lista],TempoAcumulado):-
			operacao_maquina(Tipo,Maq,_,_,_),
			(   (agenda_maq(Maq,A),extrair_tempo_acumulado(A,Tempo)); Tempo = 0),
			calcular_tempo_acumulado(Lista,Tempo2),
			(   (Tempo > Tempo2, TempoAcumulado =Tempo); TempoAcumulado = Tempo2),!.

extrair_tempo_acumulado([],0).
extrair_tempo_acumulado([t(_,Fim,_,_)|Resto],A):-
			extrair_tempo_acumulado(Resto,A1),
			(   (Fim>A1, A = Fim); A = A1).

retirar_setup_repetido([]).
retirar_setup_repetido([Tipo|ListaTipo]):-
			operacao_maquina(Tipo,Maquina,_,_,_),
			agenda_maq(Maquina,Agenda),
			retirar_setup(Agenda,a,NovaAgenda),
			retract(agenda_maq(Maquina,Agenda)),
			assertz(agenda_maq(Maquina,NovaAgenda)),
			retirar_setup_repetido(ListaTipo).


retirar_setup([],_,[]).
retirar_setup([t(A,B,C,Fer)|Resto],Ferramenta,[t(A,B,C,Fer)|NovaAgenda]):- Fer \== Ferramenta,
			C == 'setup', Ferramenta1 = Fer, retirar_setup(Resto,Ferramenta1,NovaAgenda),!.
retirar_setup([t(_,_,C,Ferramenta)|Resto],Ferramenta,NovaAgenda):- C == 'setup', retirar_setup(Resto,Ferramenta,NovaAgenda),!.
retirar_setup([t(A,B,C,D)|Resto],Ferramenta,[t(A,B,C,D)|NovaAgenda]):- C == 'exec', retirar_setup(Resto,Ferramenta,NovaAgenda),!.


menor_tempo_desl([],_,1000).
menor_tempo_desl([Tipo|Resto],N,Menor):-operacao_maquina(Tipo,Maquina,_,_,_),
			agenda_maq(Maquina,Agenda),
			obter_diferenca(Agenda,N,Men),
			menor_tempo_desl(Resto,N,Menor1),
			(   (Men<Menor1, Menor = Men);Menor=Menor1).


obter_diferenca([t(_,_,_,_)|[]],_,1000).
obter_diferenca([t(_,T2A,_,[_,_,_,_,T]),t(T1B,B,C,TipoB)|Resto],N,Diferenca):-
			N1 is N -1,
			string_concat(t,N1,T1),
			(   (T1==T,Diferenca is T1B - T2A,obter_diferenca([t(T1B,B,C,TipoB)|Resto],N,_));
			(   obter_diferenca([t(T1B,B,C,TipoB)|Resto],N,Diferenca1),
			    Diferenca = Diferenca1)).
obter_diferenca([t(_,_,_,_),t(T1B,B,C,TipoB)|Resto],N,Diferenca):- obter_diferenca([t(T1B,B,C,TipoB)|Resto],N,Diferenca1),
			Diferenca = Diferenca1.



deslizamento([],_,_):-!.
deslizamento([Tipo|Resto],N,MenorTempo):-operacao_maquina(Tipo,Maquina,_,_,_),
			agenda_maq(Maquina,Agenda),
			desl(Agenda,0,N,MenorTempo,NovaAgenda),
			retract(agenda_maq(Maquina,Agenda)),
			assertz(agenda_maq(Maquina,NovaAgenda)),
			deslizamento(Resto,N,MenorTempo).


desl([],_,_,_,[]):-!.
desl([t(TI,TF,A,[B,C,D,F,Tipo])|Resto],Trigger,N,MenorTempo,[t(TI,TF,A,[B,C,D,F,Tipo])|Nova]):- Trigger == 0,
			N1 is N -1, string_concat(t,N1,T), T==Tipo, Trigger1 = 1, desl(Resto,Trigger1,N,MenorTempo,Nova),!.
desl([t(TI,TF,A,B)|Resto],Trigger,N,MenorTempo,[t(TI,TF,A,B)|Nova]):- Trigger == 0,
			desl(Resto,Trigger,N,MenorTempo,Nova),!.
desl([t(TI,TF,A,B)|Resto],Trigger,N,MenorTempo,[t(T3,T4,A,B)|Nova]):- Trigger == 1,
			desl(Resto,Trigger,N,MenorTempo,Nova), T3 is TI - MenorTempo, T4 is TF - MenorTempo,!.

