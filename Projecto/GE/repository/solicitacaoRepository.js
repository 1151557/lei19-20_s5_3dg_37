// const mongoose = require("mongoose");
// const Encomenda = require("../model/encomenda");
var ObjectId = require('mongodb').ObjectID;
const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://chamine037:reipig69@ge-kmkj7.mongodb.net/fabrica?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });


/*POST encomenda*/
//localhost:3000/solicitacao/encomenda/Client
solicitacao_create = (req, res, next, solicitacao) => {

    try {
        client.connect(async err => {
            const db = client.db("fabrica");
            var myPromise = () => {
                return new Promise((resolve, reject) => {
                    db.collection('solicitacao')
                        .insertOne(solicitacao)
                        .then(result => {
                            res.status(201).json({
                                message: 'Solicitacao criada com sucesso.',
                            });
                        })
                });
            };
            var callMyPromise = async () => {
                var result = await (myPromise());
                return result;
            };

            callMyPromise().then(function (result) {
                res.json(result);
            });
        });
    } catch (e) {
    }
};

/*GET all solicitacao*/
//localhost:3000/solicitacao/
solicitacao_get_all = (req, res, next, ret) => {
    try {
        client.connect(async err => {
            const db = client.db("fabrica");
            var myPromise = () => {
                return new Promise((resolve, reject) => {
                    db.collection('solicitacao')
                        .find()
                        .toArray(function (err, data) {
                            err
                                ? reject(err)
                                : resolve(data);
                        });
                });
            };
            var callMyPromise = async () => {
                var result = await (myPromise());
                return result;
            };

            callMyPromise().then(function (result) {
                ret(result);
            });

        });
    } catch (e) {
    }
};

/*GET all solicitacao from Encomenda*/
//localhost:3000/solicitacao/Encomenda
solicitacao_get_byEncomenda = (req, res, next, ret) => {
    try {
        client.connect(async err => {
            const db = client.db("fabrica");
            var myPromise = () => {
                return new Promise((resolve, reject) => {
                    db.collection('solicitacao')
                        .find({ encomenda: req.params.encomenda })
                        .toArray(function (err, data) {
                            err
                                ? reject(err)
                                : resolve(data);
                        });
                });
            };
            var callMyPromise = async () => {
                var result = await (myPromise());
                return result;
            };

            callMyPromise().then(function (result) {
                console.log(result);
                console.log(req.params.id);
                ret(result);
            });

        });
    } catch (e) {
    }
};

/*GET encomenda by ID*/
//localhost:3000/solicitacao/{id}
solicitacao_get_byID = (req, res, next, ret) => {
    try {
        client.connect(async err => {
            const db = client.db("fabrica");
            var myPromise = () => {
                return new Promise((resolve, reject) => {
                    db.collection('solicitacao')
                        .find({ _id: new ObjectId(req.params.id) })
                        .toArray(function (err, data) {
                            err
                                ? reject(err)
                                : resolve(data);
                        });
                });
            };
            var callMyPromise = async () => {
                var result = await (myPromise());
                return result;
            };

            callMyPromise().then(function (result) {
                console.log(result);
                console.log(req.params.id);
                ret(result);
            });

        });
    } catch (e) {
    }
};

/*DELETE encomenda by ID*/
//localhost:3000/solicitacao/{id}
solicitacao_delete_byID = (req, res, next) => {
    try {
        client.connect(async err => {
            const db = client.db("fabrica");
            var myPromise = () => {
                return new Promise((resolve, reject) => {
                    db.collection('solicitacao')
                        .deleteOne({ _id: new ObjectId(req.params.id) })
                    return res.status(200).json({
                        message: 'Solicitacao removida com sucesso.'
                    })
                });
            };
            var callMyPromise = async () => {
                var result = await (myPromise());
                return result;
            };
            callMyPromise().then(function (result) {
                res.json(result);
            })
        })
    } catch (e) {
    }
};

solicitacao_delete_byEncomenda = (req, res, next) => {
    try {
        client.connect(async err => {
            const db = client.db("fabrica");
            var myPromise = () => {
                return new Promise((resolve, reject) => {
                    console.log(req.params.id);
                    db.collection('solicitacao')
                        .deleteOne({ encomenda: req.params.id})
                    return res.status(200).json({
                        message: 'Solicitacao removida com sucesso.'
                    })
                });
            };
            var callMyPromise = async () => {
                var result = await (myPromise());
                
                return result;
            };
            callMyPromise().then(function (result) {
                res.json(result);
            })
        })
    } catch (e) {
    }
};

module.exports = { solicitacao_create, solicitacao_get_byID, solicitacao_get_all, solicitacao_delete_byID, solicitacao_delete_byEncomenda, solicitacao_get_byEncomenda }