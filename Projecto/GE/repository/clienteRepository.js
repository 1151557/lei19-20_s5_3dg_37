const mongoose = require("mongoose");
const Cliente = require("../model/cliente");
const bcrypt = require("bcryptjs");
var jwt = require('jsonwebtoken');
var VerifyToken = require('../auth/VerifyToken');
var ObjectId = require('mongodb').ObjectID;
const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://chamine037:reipig69@ge-kmkj7.mongodb.net/fabrica?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });


createUser = (req, res, next, doc) => {
    try {
        client.connect(async err => {

            const db = client.db("fabrica");
            var myPromise = () => {
                return new Promise((resolve, reject) => {
                    db.collection('clientes').findOne({ email: req.body.email }, function (err, x) {
                        if (x) return res.status(400).send({ erro: 'Já existe um conta registada com esse e-mail. Tente outro por favor.' });
                        db.collection('clientes').findOne({ "nif": req.body.nif }, function (err, x) {
                            if (x) return res.status(400).send({ erro: 'Já existe um conta registada com esse nif. Tente outro por favor.' });
                            db.collection('clientes').insertOne(doc).then(rrrr => {
                                console.log(rrrr);
                                return res.status(200).json({
                                    message: 'Cliente registado com sucesso.'
                                })
                            });
                        });
                    });
                });
            };
            var callMyPromise = async () => {
                var result = await (myPromise());
                return result;
            };

            callMyPromise().then(function (result) {
                client.close();
                res.json(result);
            });
        });
    } catch (e) {
        console.log("ERRO");
    }
}

loginCliente = (req, res, next) => {
    try {

        client.connect(async err => {

            const db = client.db("fabrica");
            var myPromise = () => {
                return new Promise((resolve, reject) => {
                    db.collection('clientes').findOne({ email: req.body.email }, function (err, doc) {
                        if (err) throw err;
                        if (!doc) {
                            return res.status(400).send({
                                success: false,
                                message: 'E-mail não registado. Registe-se antes de fazer login.'
                            });
                        } else if (doc) {
                            if (!bcrypt.compareSync(req.body.password, doc.password))
                                return res.status(401).send({
                                    auth: false,
                                    token: null,
                                    message: 'Password incorreta. Tente novamente.'
                                });
                            else {
                                const payload = { doc: doc.email };
                                var theToken = jwt.sign(payload, 'The secret_123456789', { expiresIn: 86400 });
                                return res.json({
                                    success: 'Login bem sucedido. Bem vindo ao mundo encantado dos brinquedos.',
                                    message: 'Aproveite o seu token.',
                                    token: theToken,
                                    id: doc._id,
                                    isAdmin: doc.isAdmin
                                });
                            }
                        }
                    });
                });
            };
            var callMyPromise = async () => {
                var result = await (myPromise());
                return result;
            };

            callMyPromise().then(function (result) {
                client.close();
                res.json(result);
            });

        });
    } catch (e) {
        console.log("ERRO");
    }
};

consultarCliente = function (req, res, next, ret) {
    try {
        client.connect(async err => {
            const db = client.db("fabrica");
            var myPromise = () => {
                return new Promise((resolve, reject) => {
                    db.collection('clientes')
                        .find({ _id: new ObjectId(req.params.id) })
                        .project({ "password": 0 })
                        .toArray(function (err, c) {
                            err ? reject(err) : resolve(c);
                        });
                });
            };
            var result = await myPromise();
            console.log(result);
            ret(result);
        })
    } catch (e) {
        console.log("ERRO");
    }
};

consultarAllClientes = function (req, res, next, ret) {
    try {
        client.connect(async err => {
            const db = client.db("fabrica");
            var myPromise = () => {
                return new Promise((resolve, reject) => {
                    db.collection('clientes')
                        .find()
                        .project({ "password": 0 })
                        .toArray(function (err, data) {
                            err
                                ? reject(err)
                                : resolve(data);
                        });
                });
            };
            var result = await myPromise();
            ret(result);
        })
    } catch (e) {
        console.log("ERRO");
    }

};

alterarCliente = (req, res, next) => {
    try {
        client.connect(async err => {
            const db = client.db("fabrica");
            var myPromise = () => {
                return new Promise((resolve, reject) => {
                    var newvalues = { $set: { nome: req.body.nome, morada: req.body.morada, isAdmin: req.body.isAdmin === true } };
                    console.log(newvalues)
                    db.collection('clientes').updateOne({ _id: new ObjectId(req.params.id) }, newvalues).then(cccc => {
                        console.log(cccc);
                        return res.status(200).json({
                            message: 'Dados do cliente ABANADOR atualizados.'
                        })
                    });
                });
            };
            var callMyPromise = async () => {
                var result = await (myPromise());
                return result;
            };

            callMyPromise().then(function (result) {
                client.close();
                res.json(result);
            })
        })
    } catch (e) {
        console.log("ERRO");
    }
};

apagarCliente = function (req, res, next, ret) {
    try {
        client.connect(async err => {
            const db = client.db("fabrica");
            var myPromise = () => {
                return new Promise((resolve, reject) => {
                    try {
                        db.collection('clientes')
                            .deleteOne({ _id: new ObjectId(req.params.id) }).then(
                                x => {
                                    return res.status(200).send(x.deletedCount.toString());
                                }
                            )
                    } catch (e) {
                        print(e);
                        return res.sendStatus(500);
                    }
                });
            };
            var result = await myPromise();
            console.log(result);
            ret(result);
        })
    } catch (e) {
        console.log("ERRO");
    }
};

module.exports = { createUser, loginCliente, consultarCliente, consultarAllClientes, alterarCliente, apagarCliente }