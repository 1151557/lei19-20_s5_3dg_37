// const mongoose = require("mongoose");
// const Encomenda = require("../model/encomenda");
var ObjectId = require('mongodb').ObjectID;
const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://chamine037:reipig69@ge-kmkj7.mongodb.net/fabrica?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });


/*POST encomenda*/
//localhost:3000/encomendas/encomenda/Client
encomendas_create = (req, res, next, encomenda) => {
    try {
        client.connect(async err => {
            const db = client.db("fabrica");
            var myPromise = () => {
                return new Promise((resolve, reject) => {
                    db.collection('encomenda')
                        .insertOne(encomenda)
                        .then(result => {
                            res.status(201).json({
                                message: 'Encomenda criada com sucesso.',
                            });
                        })
                });
            };
            var callMyPromise = async () => {
                var result = await (myPromise());
                return result;
            };

            callMyPromise().then(function (result) {
                res.json(result);
            });
        });
    } catch (e) {
    }
};

/*GET all encomendas*/
//localhost:3000/encomendas/
encomendas_get_all = (req, res, next, ret) => {
    try {
        client.connect(async err => {
            const db = client.db("fabrica");
            var myPromise = () => {
                return new Promise((resolve, reject) => {
                    db.collection('encomenda')
                        .find()
                        .toArray(function (err, data) {
                            err
                                ? reject(err)
                                : resolve(data);
                        });
                });
            };
            var callMyPromise = async () => {
                var result = await (myPromise());
                return result;
            };

            callMyPromise().then(function (result) {
                ret(result);
            });

        });
    } catch (e) {
    }
};

/*GET all encomendas from client*/
//localhost:3000/encomendas/Client
encomendas_get_byClient = (req, res, next, ret) => {
    try {
        client.connect(async err => {
            const db = client.db("fabrica");
            var myPromise = () => {
                return new Promise((resolve, reject) => {
                    db.collection('encomenda')
                        .find({ cliente: req.params.cliente })
                        .toArray(function (err, data) {
                            err
                                ? reject(err)
                                : resolve(data);
                        });
                });
            };
            var callMyPromise = async () => {
                var result = await (myPromise());
                return result;
            };

            callMyPromise().then(function (result) {
                ret(result);
            });

        });
    } catch (e) {
    }
};

/*GET encomenda by ID*/
//localhost:3000/encomendas/{id}
encomendas_get_byID = (req, res, next, ret) => {
    try {
        client.connect(async err => {
            const db = client.db("fabrica");
            var myPromise = () => {
                return new Promise((resolve, reject) => {
                    db.collection('encomenda')
                        .find({ _id: new ObjectId(req.params.id) })
                        .toArray(function (err, data) {
                            err
                                ? reject(err)
                                : resolve(data);
                        });
                });
            };
            var callMyPromise = async () => {
                var result = await (myPromise());
                return result;
            };

            callMyPromise().then(function (result) {
                console.log(result);
                console.log(req.params.id);
                ret(result);
            });

        });
    } catch (e) {
    }
};

/*PUT encomenda by ID*/
//localhost:3000/encomendas/{id}
encomendas_update = (req, res, next) => {
    try {
        client.connect(async err => {
            const db = client.db("fabrica");
            var myPromise = () => {
                return new Promise((resolve, reject) => {
                    var alteracoes = {
                        $set: {
                            quantidade: req.body.quantidade,
                            data_conclusao: req.body.data_conclusao,
                            estado: req.body.estado
                        }
                    };
                    db.collection('encomenda')
                        .updateOne({ _id: new ObjectId(req.params.id) }, alteracoes)
                        .then(result => {
                            return res.status(200).json({
                                message: 'Encomenda alterada com sucesso.'
                            })
                        });
                });
            };
            var callMyPromise = async () => {
                var result = await (myPromise());
                return result;
            };

            callMyPromise().then(function (result) {
                console.log(result);
                res.json(result);
            })
        })
    } catch (e) {
    }
};

/*DELETE encomenda by ID*/
//localhost:3000/encomendas/{id}
encomendas_delete_byID = (req, res, next) => {
    try {
        client.connect(async err => {
            const db = client.db("fabrica");
            var myPromise = () => {
                return new Promise((resolve, reject) => {
                    db.collection('encomenda')
                        .deleteOne({ _id: new ObjectId(req.params.id) })
                    // var s = db.collection('solicitacao').find({encomenda: req.params.id}).toArray();
                    // db.collection('solicitacao').deleteOne({_id: s[0]._id})
                    return res.status(200).json({
                        message: 'Encomenda removida com sucesso.'
                    })
                });
            };
            var callMyPromise = async () => {
                var result = await (myPromise());
                return result;
            };
            callMyPromise().then(function (result) {
                res.json(result);
            })
        })
    } catch (e) {
    }
};

encomendas_get_all_produto_quantidade = (req, res, next, ret) => {
    try {
        client.connect(async err => {
            const db = client.db("fabrica");
            var myPromise = () => {
                return new Promise((resolve, reject) => {
                    db.collection('encomenda').find({}, { "quantidade": 1 }).sort({ "quantidade": -1 }).toArray(function (err, data) {
                        err
                            ? reject(err)
                            : resolve(data);
                    });
                });
            };
            var callMyPromise = async () => {
                var result = await (myPromise());
                return result;
            };

            callMyPromise().then(function (result) {
                ret(result);
            });

        });
    } catch (e) {
    }
};

encomendas_numero_encomendas_produto = (req, res, next, ret) => {
    try {
        client.connect(async err => {
            const db = client.db("fabrica");
            var myPromise = () => {
                return new Promise((resolve, reject) => {
                    db.collection('encomenda').aggregate(
                        [
                            { $match: {} },
                            {
                                $group: {
                                    _id: "$produto",
                                    count: { $sum: 1 }
                                }
                            },
                            { $sort: { "count": -1 } }]
                    ).toArray(function (err, data) {
                        err
                            ? reject(err)
                            : resolve(data);
                    })
                })
            }
            var callMyPromise = async () => {
                var result = await (myPromise());
                return result;
            };

            callMyPromise().then(function (result) {
                ret(result);
            });

        });
    } catch (e) {
    }
};

/*DELETE encomenda by ID*/
//localhost:3000/encomendas/{id}
encomendas_delete_byCliente = (req, res, next) => {
    try {
        client.connect(async err => {
            const db = client.db("fabrica");
            var myPromise = () => {
                return new Promise((resolve, reject) => {
                    var alteracoes = {
                        $set: {
                            estado: "Inativa"
                        }
                    };
                    console.log(req)
                    try
                    {
                    db.collection('encomenda')
                        .updateMany({ cliente: req.params.id }, alteracoes)
                        .then(result => {
                            return res.status(200).json({
                                message: 'Encomendas alteradas com sucesso.'
                            })
                        });
                    }
                    catch(e)
                    {
                        console.log(e)
                    }
                });
            };
            var callMyPromise = async () => {
                var result = await (myPromise());
                return result;
            };

            callMyPromise().then(function (result) {
                console.log(result);
                res.json(result);
            })
        })
    } catch (e) {
        console.log(e)
    }
};

/*GET all encomendas de produto*/
//localhost:3000/encomendas/{id_produto}
// encomendas_get_all_produto = (req, res, next, ret) => {
//     try {
//         client.connect(async err => {
//             const db = client.db("fabrica");
//             var myPromise = () => {
//                 return new Promise((resolve, reject) => {
//                     db.collection('encomenda').find({ "produto": req.params.id }).count().then((count) => {
//                         res.status(200).json({
//                             message: 'NÚMERO DE ENCOMENDAS DO PRODUTO COM O ID ' + req.params.id + ' é ' + count,
//                         });
//                         //console.log("NÚMERO DE ENCOMENDAS DO PRODUTO COM O ID " + req.params.id + " é " + count);
//                     })


//                 });
//             };
//             var callMyPromise = async () => {
//                 var result = await (myPromise());
//                 return result;
//             };

//             callMyPromise().then(function (result) {
//                 ret(result);
//             });

//         });
//     } catch (e) {
//     }
// };

module.exports = { encomendas_create, encomendas_get_byID, encomendas_get_all, encomendas_get_byClient, encomendas_delete_byID, encomendas_update, encomendas_get_all_produto_quantidade, encomendas_numero_encomendas_produto, encomendas_delete_byCliente }