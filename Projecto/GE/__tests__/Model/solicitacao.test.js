const SolicitacaoDTO = require("../../model/DTO/solicitacaoDTO");

const s = {
    "_id": "789",
    "encomenda": "id_encomenda",
    "produto": "produto teste",
    "cliente": "Cliente da solicitacao",
    "data_solicitacao": "2019-12-05",
    "nova_quantidade": 20,
    "nova_data_conclusao": "2019-12-31",
    "cancelar_encomenda": false
};
const sSolicitacao = new SolicitacaoDTO({
    "_id": "789",
    "encomenda": "id_encomenda",
    "produto": "produto teste",
    "cliente": "Cliente da solicitacao",
    "data_solicitacao": "2019-12-05",
    "nova_quantidade": 20,
    "nova_data_conclusao": "2019-12-31",
    "cancelar_encomenda": false
});

describe('TESTES UNITÁRIOS SOLICITAÇÃO', () => {

    test('teste solicitar alteração encomenda', () => {
        expect(sSolicitacao._id).toEqual(s._id);
        expect(sSolicitacao.encomenda).toEqual(s.encomenda);
        expect(sSolicitacao.produto).toEqual(s.produto);
        expect(sSolicitacao.cliente).toEqual(s.cliente);
        expect(sSolicitacao.data_solicitacao).toEqual(expect.any(Date));
        expect(sSolicitacao.nova_quantidade).toEqual(s.nova_quantidade);
        expect(sSolicitacao.nova_data_conclusao).toEqual(expect.any(Date));
        expect(sSolicitacao.cancelar_encomenda).toEqual(s.cancelar_encomenda);
    });

});