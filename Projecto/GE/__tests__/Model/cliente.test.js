const ClienteDTO = require("../../model/DTO/clienteDTO");

const c = {
    "_id": "123",
    "nome": "Nome teste",
    "email": "teste@gmail.com",
    "password": "passpass",
    "morada": "Rua dos testes, 423",
    "nif": 123456789,
    "isAdmin": false
};
const cCliente = new ClienteDTO({
    "_id": "123",
    "nome": "Nome teste",
    "email": "teste@gmail.com",
    "password": "passpass",
    "morada": "Rua dos testes, 423",
    "nif": 123456789,
    "isAdmin": false
});

describe('TESTES UNITÁRIOS CLIENTE', () => {

    test('teste model cliente', () => {
        expect(cCliente).toEqual(expect.objectContaining(c));
    });

});