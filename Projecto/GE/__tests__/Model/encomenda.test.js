const EncomendaDTO = require("../../model/DTO/endomendaDTO");

const e = {
    "_id": "456",
    "cliente": "Cliente da encomenda",
    "produto": "produto teste",
    "quantidade": 10,
    "data_conclusao": "2019-12-24",
    "estado": "ativo"
};
const eEncomenda = new EncomendaDTO({
    "_id": "456",
    "cliente": "Cliente da encomenda",
    "produto": "produto teste",
    "quantidade": 10,
    "data_conclusao": "2019-12-24",
    "estado": "ativo"
});

describe('TESTES UNITÁRIOS ENCOMENDA', () => {

    test('teste criar encomenda', () => {
        expect(eEncomenda).toEqual(expect.objectContaining(e));
    });

});