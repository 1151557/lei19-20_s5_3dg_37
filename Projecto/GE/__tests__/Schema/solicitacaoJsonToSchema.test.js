const SolicitacaoMappers = require("../../mappers/solicitacaoMappers");

const s = {
    body: {
        _id: "789",
        encomenda: "id_encomenda",
        produto: "produto teste",
        cliente: "Cliente da solicitacao",
        data_solicitacao: "2019-12-05",
        nova_quantidade: 20,
        nova_data_conclusao: "2019-12-31",
        cancelar_encomenda: false
    }
};


const expected = {
    _id: "789",
    encomenda: "id_encomenda",
    produto: "produto teste",
    cliente: "Cliente da solicitacao",
    data_solicitacao: "2019-12-05T00:00:00.000Z",
    nova_quantidade: 20,
    nova_data_conclusao: "2019-12-31",
    cancelar_encomenda: false
}

describe('TESTES UNITÁRIOS SOLICITACAO SCHEMA', () => {

    test('teste json to schema do cliente', () => {
        var ret = SolicitacaoMappers.solicitacaoJsonToSchema(s);
        expect(ret.encomenda).toEqual(expected.encomenda)
        expect(ret.produto).toEqual(expected.produto)
        expect(ret.cliente).toEqual(expected.cliente)
        expect(ret.data_solicitacao).toEqual(expect.any(Date))
        expect(ret.nova_quantidade).toEqual(expected.nova_quantidade)
        expect(ret.nova_data_conclusao).toEqual(expect.any(Date))
        expect(ret.cancelar_encomenda).toEqual(expected.cancelar_encomenda)
    });
});