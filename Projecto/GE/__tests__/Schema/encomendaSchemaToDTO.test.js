const EncomendaMappers = require("../../mappers/encomendaMappers");
const Encomenda = require("../../model/encomenda");
const EncomendaDTO = require("../../model/DTO/endomendaDTO");
const ObjectId = require('mongodb').ObjectID;

var a = new ObjectId();

var e = [
    new Encomenda({
        _id: a,
        cliente: "Cliente da encomenda",
        produto: "produto teste",
        quantidade: 10,
        data_conclusao: "2019-12-24",
        estado: "Ativa"
    })
]


const expected =
    new EncomendaDTO({
        _id: a,
        cliente: "Cliente da encomenda",
        produto: "produto teste",
        quantidade: 10,
        data_conclusao: "2019-12-24",
        estado: "Ativa"
    })

describe('TESTES UNITÁRIOS ENCOMENDA SCHEMA', () => {

    test('teste schema to DTO do encomenda', () => {
        var ret = EncomendaMappers.encomendaSchemaToDTO(e);
        expect(ret._id).toEqual(expected._id)
        expect(ret.cliente).toEqual(expected.cliente)
        expect(ret.produto).toEqual(expected.produto)
        expect(ret.quantidade).toEqual(expected.quantidade)
        expect(ret.data_conclusao).toEqual(expected.data_conclusao)
        expect(ret.estado).toEqual(expected.estado)
    });
});