const ClienteMappers = require("../../mappers/clienteMappers");

const c = {
    body: {
        nome: "Nome teste",
        morada: "Rua dos testes, 423",
        nif: "123456789",
        email: "teste@gmail.com",
        password: "passpass",
        isAdmin: false,
        confirmPassword: "passpass"
    }
};

const expected = {
    nome: 'Nome teste',
    morada: "Rua dos testes, 423",
    email: 'teste@gmail.com',
    password: "$2a$08$igiDBB8fdQmAzWMeuslIy.KfhDSv3CEM1B1eYfh0wX7eqeD7f4TUi",
    nif: '123456789',
    isAdmin: false
}

describe('TESTES UNITÁRIOS CLIENTE SCHEMA', () => {

    test('teste json to schema do cliente', () => {
        var ret = ClienteMappers.userJsonToSchema(c);
        expect(ret.nome).toEqual(expected.nome)
        expect(ret.morada).toEqual(expected.morada)
        expect(ret.email).toEqual(expected.email)
        expect(ret.password).toEqual(expect.any(String))
        expect(ret.nif).toEqual(expected.nif)
        expect(ret.isAdmin).toEqual(expected.isAdmin)
    });
});