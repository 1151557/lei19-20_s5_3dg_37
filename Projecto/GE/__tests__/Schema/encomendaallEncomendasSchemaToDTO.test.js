const EncomendaMappers = require("../../mappers/encomendaMappers");
const Encomenda = require("../../model/encomenda");
const EncomendaDTO = require("../../model/DTO/endomendaDTO");
const ObjectId = require('mongodb').ObjectID;

var a = new ObjectId();

var e = [
    new Encomenda({
        _id: a,
        cliente: "Cliente da encomenda",
        produto: "produto teste",
        quantidade: 10,
        data_conclusao: "2019-12-24",
        estado: "Ativa"
    }),
    new Encomenda({
        _id: a,
        cliente: "Cliente da encomenda 123",
        produto: "produto teste 123",
        quantidade: 20,
        data_conclusao: "2019-12-24",
        estado: "Ativa"
    })
]


const expected =
    new EncomendaDTO({
        _id: a,
        cliente: "Cliente da encomenda",
        produto: "produto teste",
        quantidade: 10,
        data_conclusao: "2019-12-24",
        estado: "Ativa"
    })

describe('TESTES UNITÁRIOS ENCOMENDA SCHEMA', () => {

    test('teste all schema to DTO do encomenda', () => {
        var ret = EncomendaMappers.allEncomendasSchemaToDTO(e);
        expect(ret[0]._id).toEqual(expected._id)
        expect(ret[0].cliente).toEqual(expected.cliente)
        expect(ret[0].produto).toEqual(expected.produto)
        expect(ret[0].quantidade).toEqual(expected.quantidade)
        expect(ret[0].data_conclusao).toEqual(expected.data_conclusao)
        expect(ret[0].estado).toEqual(expected.estado)
    });
});