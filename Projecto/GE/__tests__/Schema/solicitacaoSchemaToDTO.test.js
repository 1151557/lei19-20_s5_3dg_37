const SolicitacaoMappers = require("../../mappers/solicitacaoMappers");
const Solicitacao = require("../../model/solicitacao");
const SolicitacaoDTO = require("../../model/DTO/solicitacaoDTO");

var s = [
    new Solicitacao({
        _id: "789",
        encomenda: "id_encomenda",
        produto: "produto teste",
        cliente: "Cliente da solicitacao",
        data_solicitacao: "2019-12-05",
        nova_quantidade: 20,
        nova_data_conclusao: "2019-12-31",
        cancelar_encomenda: false
    })
]


const expected =
    new SolicitacaoDTO({
        _id: "789",
        encomenda: "id_encomenda",
        produto: "produto teste",
        cliente: "Cliente da solicitacao",
        data_solicitacao: "2019-12-05",
        nova_quantidade: 20,
        nova_data_conclusao: "2019-12-31",
        cancelar_encomenda: false
    })

describe('TESTES UNITÁRIOS SOLICITACAO SCHEMA', () => {

    test('teste schema to DTO da solicitacao', () => {
        var ret = SolicitacaoMappers.solicitacaoSchemaToDTO(s);
        expect(ret.encomenda).toEqual(expected.encomenda)
        expect(ret.produto).toEqual(expected.produto)
        expect(ret.cliente).toEqual(expected.cliente)
        expect(ret.data_solicitacao).toEqual(expect.any(Date))
        expect(ret.nova_quantidade).toEqual(expected.nova_quantidade)
        expect(ret.nova_data_conclusao).toEqual(expect.any(Date))
        expect(ret.cancelar_encomenda).toEqual(expected.cancelar_encomenda)
    });
});