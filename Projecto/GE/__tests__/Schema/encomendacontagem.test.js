const EncomendaMappers = require("../../mappers/encomendaMappers");
const Encomenda = require("../../model/encomenda");
const EncomendaDTO = require("../../model/DTO/endomendaDTO");
const ProdutoDTO = require('../../model/DTO/produtoDTO');


const p =
    new ProdutoDTO({
        _id: "123",
        count: 10
    })

var e = [
    new EncomendaDTO({
        _id: "5444",
        cliente: "Cliente da encomenda 123",
        produto: p,
        quantidade: 20,
        data_conclusao: "2019-12-24",
        estado: "Ativa"
    })
]


const expected =
    new ProdutoDTO({
        _id: "123",
        count: 10
    })

describe('TESTES UNITÁRIOS ENCOMENDA SCHEMA', () => {

    test('teste constagem produto all schema to DTO do encomenda', () => {
        var ret = EncomendaMappers.contagemProdutoSchemaToDTO(e);
        expect(ret.produto._id).toEqual(expected._id)
        expect(ret.produto.count).toEqual(expected.count)
    });
});