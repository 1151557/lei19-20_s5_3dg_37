const EncomendaMappers = require("../../mappers/encomendaMappers");
const ObjectId = require('mongodb').ObjectID;

var a = new ObjectId();

const aaa = {
    headers: {
        cliente: "Cliente da encomenda"
    }
}

const e = {
    body: {
        _id: a,
        cliente: aaa,
        produto: "produto teste",
        quantidade: 10,
        data_conclusao: "2019-12-24",
        estado: "Ativa"
    }
};

const expected = {
    _id: a,
    cliente: aaa,
    produto: "produto teste",
    quantidade: 10,
    data_conclusao: "2019-12-24",
    estado: "Ativa"
}

describe('TESTES UNITÁRIOS ENCOMENDA SCHEMA', () => {

    test('teste json to schema da encomenda', () => {
        var ret = EncomendaMappers.encomendaJsonToSchema(e);
        console.log("AAA", aaa)
        expect(ret._id).toEqual(expected._id)
        expect(ret.cliente).toEqual(expected.cliente)
        expect(ret.quantidade).toEqual(expected.quantidade)
        expect(ret.data_conclusao).toEqual(expected.data_conclusao)
        expect(ret.estado).toEqual(expected.estado)
    });
});