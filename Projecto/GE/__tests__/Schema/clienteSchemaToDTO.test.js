const ClienteMappers = require("../../mappers/clienteMappers");
const Cliente = require("../../model/cliente");
const ClienteDTO = require("../../model/DTO/clienteDTO");

var c = [
    new Cliente({
        _id: "5df2ba3f92d22f7220a70201",
        nome: "Nome teste",
        morada: "Rua dos testes, 423",
        nif: "123456789",
        email: "teste@gmail.com",
        password: "passpass",
        isAdmin: false
    })
]


const expected =
    new ClienteDTO({
        _id: '5df2ba3f92d22f7220a70201',
        nome: 'Nome teste',
        morada: 'Rua dos testes, 423',
        email: 'teste@gmail.com',
        password: '$2a$08$igiDBB8fdQmAzWMeuslIy.KfhDSv3CEM1B1eYfh0wX7eqeD7f4TUi',
        nif: '123456789',
        isAdmin: false
    })

describe('TESTES UNITÁRIOS CLIENTE SCHEMA', () => {

    test('teste schema to DTO do cliente', () => {
        var ret = ClienteMappers.userSchemaToDTO(c);
        expect(ret._id).toEqual(expected._id)
        expect(ret.nome).toEqual(expected.nome)
        expect(ret.email).toEqual(expected.email)
        expect(ret.password).toEqual(expect.any(String))
        expect(ret.nif).toEqual(expected.nif)
        expect(ret.isAdmin).toEqual(expected.isAdmin)
    });
});