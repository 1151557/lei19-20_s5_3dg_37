const SolicitacaoMappers = require("../../mappers/solicitacaoMappers");
const Solicitacao = require("../../model/solicitacao");
const SolicitacaoDTO = require("../../model/DTO/solicitacaoDTO");

var s = [
    new Solicitacao({
        _id: "789",
        encomenda: "id_encomenda",
        produto: "produto teste",
        cliente: "Cliente da solicitacao",
        data_solicitacao: "2019-12-05",
        nova_quantidade: 20,
        nova_data_conclusao: "2019-12-31",
        cancelar_encomenda: false
    }),
    new Solicitacao({
        _id: "356",
        encomenda: "id_encomenda 123",
        produto: "produto teste 123",
        cliente: "Cliente da solicitacao 123",
        data_solicitacao: "2019-12-07",
        nova_quantidade: 20,
        nova_data_conclusao: "2019-12-15",
        cancelar_encomenda: false
    })
]


const expected =
    new SolicitacaoDTO({
        _id: "789",
        encomenda: "id_encomenda",
        produto: "produto teste",
        cliente: "Cliente da solicitacao",
        data_solicitacao: "2019-12-05",
        nova_quantidade: 20,
        nova_data_conclusao: "2019-12-31",
        cancelar_encomenda: false
    })

describe('TESTES UNITÁRIOS SOLICITACAO SCHEMA', () => {

    test('teste all schema to DTO da solicitacao', () => {
        var ret = SolicitacaoMappers.allSolicitacaoSchemaToDTO(s)
        expect(ret[0].encomenda).toEqual(expected.encomenda)
        expect(ret[0].produto).toEqual(expected.produto)
        expect(ret[0].cliente).toEqual(expected.cliente)
        expect(ret[0].data_solicitacao).toEqual(expect.any(Date))
        expect(ret[0].nova_quantidade).toEqual(expected.nova_quantidade)
        expect(ret[0].nova_data_conclusao).toEqual(expect.any(Date))
        expect(ret[0].cancelar_encomenda).toEqual(expected.cancelar_encomenda)
    });
});