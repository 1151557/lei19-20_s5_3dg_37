const mongoose = require('mongoose');

const produtoDTO = mongoose.Schema({
    _id: {
        type: String
    },
    count: {
        type: Number,
    }
});

module.exports = mongoose.model('produtoDTO', produtoDTO);