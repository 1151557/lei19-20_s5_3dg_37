const mongoose = require('mongoose');

const clienteDTO = mongoose.Schema({

    _id: { 
        type: String 
    },
    nome: {
        type: String,
    },
    email: {
        type: String,
    },
    password: {
        type: String,
    },
    morada: {
        type: String,
    },
    nif: {
        type: Number,
    },
    isAdmin: {
        type: Boolean,
    }
});

module.exports = mongoose.model('clienteDTO', clienteDTO);