const mongoose = require('mongoose');

const solicitacaoDTOSchema = mongoose.Schema({
    _id: {
        type: String 
    },
    encomenda: {
        type: String //id da encomenda
    },
    produto: {
        type: String //nome
    },
    cliente: {
        type: String //nome
    },
    data_solicitacao: {
        type: Date
    },
    nova_quantidade: {
        type: Number
    },
    nova_data_conclusao: {
        type: Date
    },
    cancelar_encomenda: {
        type: Boolean,
    }

});

module.exports = mongoose.model('solicitacaoDTO', solicitacaoDTOSchema);