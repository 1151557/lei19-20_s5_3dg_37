const mongoose = require('mongoose');

const encomendaDTO = mongoose.Schema({
    _id: { 
        type: String 
    },
    cliente: {
        type: String,
    },
    produto: {
        type: String,
    },
    quantidade: {
        type: Number,
    },
    data_conclusao: {
        type: String,
    },
    estado: {
        type: String,
    }
});

module.exports = mongoose.model('encomendaDTO', encomendaDTO);