const mongoose = require('mongoose');
const Estados = Object.freeze({
    ATIVO: 'ativa',
    INATIVO: 'inativa',
});

const encomendaSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    cliente: {
        type: String,
        // ref: 'Cliente',
        required: true
    },
    produto: {
        type: String,
        // ref: 'Produto',
        required: true
    },
    quantidade: {
        type: Number,
        default: 1
    },
    data_conclusao: {
        type: String,
        required: true
    },
    estado: {
        type: String,
        enum: Object.values(Estados)
    }
});

module.exports = mongoose.model('Encomenda', encomendaSchema);