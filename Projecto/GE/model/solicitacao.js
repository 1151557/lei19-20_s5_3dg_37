const mongoose = require('mongoose');
const Encomenda = require('./encomenda');

const solicitacaoSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    encomenda:{
        type: String,
        require:true
    },
    data_solicitacao:{
        type: Date,
        require: true
    },
    produto: {
        type: String, //nome
        require: true
    },
    cliente: {
        type: String, //nome
        require: true
    },
    nova_quantidade: {
        type: Number
    },
    nova_data_conclusao: {
        type: Date
    },
    cancelar_encomenda:{
        type: Boolean,
        default: false
    }

});

module.exports = mongoose.model('Solicitacao', solicitacaoSchema);