const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    nome: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 30
        
    },
    email: {
        type: String,
        required: true,
        unique: true,
        minlength: 5,
        maxlength: 200,
        match: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    },
    password: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 200
    },
    morada: {
        type: String,
        required: true
    },
    nif: {
        type: String,
        required: true,
        unique: true,
        minlength: 9,
        maxlength: 9
    },
    isAdmin: {
        type: Boolean,
        required: true
    }
});

module.exports = mongoose.model('Cliente', userSchema);