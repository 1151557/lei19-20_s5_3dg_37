// const mongoose = require("mongoose");
// const Encomenda = require("../model/encomenda");
// var ObjectId = require('mongodb').ObjectID;
const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://chamine037:reipig69@ge-kmkj7.mongodb.net/fabrica?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
const service = require('../service/encomendaService');


/*POST encomenda*/
//localhost:3000/encomendas/encomenda/Client
exports.encomendas_create = (req, res, next) => {
    service.encomendas_create(req, res, next);
};

/*GET all encomendas*/
//localhost:3000/encomendas/
exports.encomendas_get_all = (req, res, next) => {
    service.encomendas_get_all(req, res, next);
};

/*GET all encomendas de produto*/
//localhost:3000/encomendas/id_produto
exports.encomendas_get_all_produto_quantidade = (req, res, next) => {
    service.encomendas_get_all_produto_quantidade(req, res, next);
};

/*GET número de vezes encomendado */
//localhost:3000/encomendas/nprodutos
exports.encomendas_numero_encomendas_produto = (req, res, next) => {
    service.encomendas_numero_encomendas_produto(req, res, next);
};

/*GET all encomendas from client*/
//localhost:3000/encomendas/{Client}
exports.encomendas_get_byClient = (req, res, next) => {
    service.encomendas_get_byClient(req, res, next);
};


/*GET encomenda by ID*/
//localhost:3000/encomendas/{id}
exports.encomendas_get_byID = (req, res, next) => {
    service.encomendas_get_byID(req, res, next);
};

/*PUT encomenda by ID*/
//localhost:3000/encomendas/{id}
exports.encomendas_update = (req, res, next) => {
    service.encomendas_update(req, res, next);
};

/*DELETE encomenda by ID*/
//localhost:3000/encomendas/{id}
exports.encomendas_delete_byID = (req, res, next) => {
    service.encomendas_delete_byID(req, res, next);
};

/* Delete encomenda by user id. */
//localhost:3000/encomendas/deletebycliente/{id}
exports.encomendas_delete_byCliente = (req, res, next) => {
    service.encomendas_delete_byCliente(req, res, next);
};