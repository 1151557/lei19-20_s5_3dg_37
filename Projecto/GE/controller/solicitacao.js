// const mongoose = require("mongoose");
// const Encomenda = require("../model/encomenda");
// var ObjectId = require('mongodb').ObjectID;
const service = require('../service/solicitacaoService');


/*POST encomenda*/
//localhost:3000/solicitacao/encomenda/Client
exports.solicitacao_create = (req, res, next) => {
    service.solicitacao_create(req,res,next);
};

/*GET all solicitacao*/
//localhost:3000/solicitacao/
exports.solicitacao_get_all = (req, res, next) => {
    service.solicitacao_get_all(req,res,next);
};

/*GET all solicitacao by encomenda id*/
//localhost:3000/solicitacao/solicitacao/{id}
exports.solicitacao_get_byEncomenda = (req, res, next) => {
    service.solicitacao_get_byEncomenda(req,res,next);
};


/*GET encomenda by ID*/
//localhost:3000/solicitacao/{id}
exports.solicitacao_get_byID = (req, res, next) => {
    service.solicitacao_get_byID(req,res,next);
};
/*DELETE encomenda by ID*/
//localhost:3000/solicitacao/{id}
exports.solicitacao_delete_byID = (req, res, next) => {
    service.solicitacao_delete_byID(req,res,next);
};

/*DELETE encomenda by encomenda ID*/
//localhost:3000/solicitacao/solicitacao/{id}
exports.solicitacao_delete_byEncomenda = (req, res, next) => {
    service.solicitacao_delete_byEncomenda(req,res,next);
};