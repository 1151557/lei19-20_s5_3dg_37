// const mongoose = require("mongoose");
// const Cliente = require("../model/cliente");
// const bcrypt = require("bcryptjs");
// var jwt = require('jsonwebtoken');
// var VerifyToken = require('../auth/VerifyToken');
// var ObjectId = require('mongodb').ObjectID;
// const MongoClient = require('mongodb').MongoClient;
// const uri = "mongodb+srv://chamine037:reipig69@ge-kmkj7.mongodb.net/fabrica?retryWrites=true&w=majority";
// const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
// const repo = require('../repository/clienteRepositrory');
const service = require('../service/clienteService');

exports.signUpCliente = (req, res, next) => {
    service.createUser(req,res,next);
};

exports.loginCliente = (req, res, next) => {
    service.loginCliente(req,res,next);
};

exports.consultarCliente = (req, res, next) => {
    service.consultarCliente(req,res,next);
};

exports.consultarAllClientes = (req, res, next) => {
    service.consultarAllClientes(req,res,next);
};

exports.alterarCliente = (req, res, next) => {
   service.alterarCliente(req,res,next);
};

exports.apagarCliente = (req, res, next) => {
    service.apagarCliente(req,res,next);
 };
