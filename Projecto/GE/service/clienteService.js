const mongoose = require("mongoose");
const Cliente = require("../model/cliente");
const bcrypt = require("bcryptjs");
var jwt = require('jsonwebtoken');
var VerifyToken = require('../auth/VerifyToken');
var ObjectId = require('mongodb').ObjectID;
const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://chamine037:reipig69@ge-kmkj7.mongodb.net/fabrica?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
const repo = require('../repository/clienteRepository');
const mapper = require('../mappers/clienteMappers');
const clienteDTO = require('../model/DTO/clienteDTO');

createUser = (req, res, next) => {
    doc = mapper.userJsonToSchema(req);
    repo.createUser(req, res, next, doc);
}

loginCliente = (req, res, next) => {
    repo.loginCliente(req, res, next);
};

consultarCliente = (req, res, next) => {
    repo.consultarCliente(req, res, next, function (result) {
        var x = mapper.userSchemaToDTO(result)
        res.send(x);
    });
};

consultarAllClientes = (req, res, next) => {
    repo.consultarAllClientes(req, res, next, function (result) {
        var x = mapper.allUsersSchemaToDTO(result);
        res.send(x);
    });
};

alterarCliente = (req, res, next) => {
    repo.alterarCliente(req, res, next);
};

apagarCliente = (req, res, next) => {
    repo.apagarCliente(req, res, next);
};

module.exports = { createUser, loginCliente, alterarCliente, consultarCliente, consultarAllClientes, apagarCliente }