const mongoose = require("mongoose");
const Encomenda = require("../model/encomenda");
const MongoClient = require('mongodb').MongoClient;
const uri = "mongodb+srv://chamine037:reipig69@ge-kmkj7.mongodb.net/fabrica?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
const repo = require('../repository/encomendaRepository');
const mapper = require('../mappers/encomendaMappers');



/*POST encomenda*/
//localhost:3000/encomendas/encomenda/Client
encomendas_create = (req, res, next) => {
    const encomenda = mapper.encomendaJsonToSchema(req);
    repo.encomendas_create(req, res, next, encomenda);
};

/*GET all encomendas*/
//localhost:3000/encomendas/
encomendas_get_all = (req, res, next) => {
    repo.encomendas_get_all(req, res, next, function (result) {
        var x = mapper.allEncomendasSchemaToDTO(result);
        res.json(x);
    });
};

/*GET all encomendas*/
//localhost:3000/encomendas/
encomendas_get_all_produto_quantidade = (req, res, next) => {
    repo.encomendas_get_all_produto_quantidade(req, res, next, function (result) {
        var x = mapper.allEncomendasSchemaToDTO(result);
        res.json(x);
    });
};
/*GET numero encomendas produto*/
//localhost:3000/encomendas/nprodutos
encomendas_numero_encomendas_produto = (req, res, next) => {
    repo.encomendas_numero_encomendas_produto(req, res, next, function (result) {
        var x = mapper.contagemProdutoSchemaToDTO(result);
        res.json(x);
    });
};

/*GET encomenda by ID*/
//localhost:3000/encomendas/{id}
encomendas_get_byID = (req, res, next) => {
    repo.encomendas_get_byID(req, res, next, function (result) {
        var x = mapper.encomendaSchemaToDTO(result);
        res.json(x)
    });
};

/*GET encomenda by ID*/
//localhost:3000/encomendas/{id}
encomendas_get_byClient = (req, res, next) => {
    repo.encomendas_get_byClient(req, res, next, function (result) {
        var x = mapper.allEncomendasSchemaToDTO(result)
        res.json(x);
    });
};

/*PUT encomenda by ID*/
//localhost:3000/encomendas/{id}
encomendas_update = (req, res, next) => {
    repo.encomendas_update(req, res, next);
};

/*DELETE encomenda by ID*/
//localhost:3000/encomendas/{id}
encomendas_delete_byID = (req, res, next) => {
    repo.encomendas_delete_byID(req, res, next);
};

/* Delete encomenda by user id. */
//localhost:3000/encomendas/deletebycliente/{id}
encomendas_delete_byCliente = (req, res, next) => {
    repo.encomendas_delete_byCliente(req, res, next);
};

module.exports = { encomendas_create, encomendas_delete_byID, encomendas_get_all, encomendas_get_byClient, encomendas_get_byID, encomendas_update, encomendas_get_all_produto_quantidade, encomendas_numero_encomendas_produto, encomendas_delete_byCliente }