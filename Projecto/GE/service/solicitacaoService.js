const repo = require('../repository/solicitacaoRepository');
const mapper = require('../mappers/solicitacaoMappers');



/*POST solicitacao*/
//localhost:3000/solicitacao/solicitacao/Client
solicitacao_create = (req, res, next) => {
    const solicitacao = mapper.solicitacaoJsonToSchema(req);
    repo.solicitacao_create(req, res, next, solicitacao);
};

/*GET all solicitacao*/
//localhost:3000/solicitacao/
solicitacao_get_all = (req, res, next) => {
    repo.solicitacao_get_all(req, res, next, function (result) {
        var x = mapper.allSolicitacaoSchemaToDTO(result);
        res.json(x);
    });
};

/*GET solicitacao by ID*/
//localhost:3000/solicitacao/{id}
solicitacao_get_byID = (req, res, next) => {
    repo.solicitacao_get_byID(req, res, next, function (result) {
        var x = mapper.solicitacaoSchemaToDTO(result);
        res.json(x)
    });
};

/*GET solicitacao by encomenda ID*/
//localhost:3000/solicitacao/solicitacao/{id}
solicitacao_get_byEncomenda = (req, res, next) => {
    repo.solicitacao_get_byEncomenda(req, res, next, function (result) {
        var x = mapper.allSolicitacaoSchemaToDTO(result)
        res.json(x);
    });
};

/*DELETE solicitacao by ID*/
//localhost:3000/solicitacao/{id}
solicitacao_delete_byID = (req, res, next) => {
    repo.solicitacao_delete_byID(req, res, next);
};

/*DELETE solicitacao by encomenda ID*/
//localhost:3000/solicitacao/solicitacao/{id}
solicitacao_delete_byEncomenda = (req, res, next) => {
    repo.solicitacao_delete_byEncomenda(req, res, next);
};

module.exports = { solicitacao_create, solicitacao_delete_byID, solicitacao_get_all, solicitacao_get_byID, solicitacao_get_byEncomenda, solicitacao_delete_byEncomenda }