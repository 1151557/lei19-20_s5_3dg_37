const Encomenda = require('../model/encomenda');
const ObjectId = require('mongodb').ObjectID;
const EncomendaDTO = require('../model/DTO/endomendaDTO');
const ProdutoDTO = require('../model/DTO/produtoDTO');


encomendaJsonToSchema = function (req) {
    const encomenda = new Encomenda({
        _id: new ObjectId(),
        cliente: req.headers.cliente,
        produto: req.body.produto,
        quantidade: req.body.quantidade,
        data_conclusao: req.body.data_conclusao,
        estado: "Ativa"
    });
    return encomenda;
}

encomendaSchemaToDTO = function (result) {
    const encDTO = new EncomendaDTO({
        _id: result[0]._id,
        cliente: result[0].cliente,
        produto: result[0].produto,
        quantidade: result[0].quantidade,
        data_conclusao: result[0].data_conclusao,
        estado: result[0].estado
    });
    return encDTO;
}

contagemProdutoSchemaToDTO = function (result) {
    var i = 0;
    const array = [];
    console.log(result)
    for (i; i < result.length; i++) {
        const pDTO = new ProdutoDTO({
            _id: result[i]._id,
            count: result[i].count
        });
        console.log(result[i], " CU")
        array.push(pDTO);
    }
    return array;
}

allEncomendasSchemaToDTO = function (result) {
    var i = 0;
    const array = [];
    for (i; i < result.length; i++) {
        const encDTO = new EncomendaDTO({
            _id: result[i]._id,
            cliente: result[i].cliente,
            produto: result[i].produto,
            quantidade: result[i].quantidade,
            data_conclusao: result[i].data_conclusao,
            estado: result[i].estado
        });
        array.push(encDTO);
    }
    return array;
}

module.exports = { encomendaJsonToSchema, allEncomendasSchemaToDTO, encomendaSchemaToDTO, contagemProdutoSchemaToDTO }