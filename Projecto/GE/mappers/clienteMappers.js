const Cliente = require("../model/cliente");
const bcrypt = require("bcryptjs");
const clienteDTO = require("../model/DTO/clienteDTO");


userJsonToSchema = function (req) {
    doc = new Cliente({
        nome: req.body.nome,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 8),
        nif: req.body.nif,
        morada: req.body.morada,
        isAdmin: req.body.isAdmin
    });
    return doc;
}

userSchemaToDTO = function (result) {
    var x = clienteDTO({
        _id: result[0]._id,
        nome: result[0].nome,
        email: result[0].email,
        password: result[0].password,
        nif: result[0].nif,
        morada: result[0].morada,
        isAdmin: result[0].isAdmin
    })
    return x;
}

allUsersSchemaToDTO = function (result) {
    var array = [];
    var i = 0
    for (i; i < result.length; i++) {
        var x = clienteDTO({
            _id: result[i]._id,
            nome: result[i].nome,
            email: result[i].email,
            password: result[i].password,
            nif: result[i].nif,
            morada: result[i].morada,
            isAdmin: result[i].isAdmin
        })
        array.push(x);
    }
    return array;
}


module.exports = { userJsonToSchema, userSchemaToDTO, allUsersSchemaToDTO }