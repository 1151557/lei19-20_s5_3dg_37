const Solicitacao = require('../model/solicitacao');
const ObjectId = require('mongodb').ObjectID;
const SolicitacaoDTO = require('../model/DTO/solicitacaoDTO');
const encomendaService = require('../service/encomendaService');


solicitacaoJsonToSchema = function (req) {
    const solicitacao = new Solicitacao({
        _id: new ObjectId(),
        encomenda: req.body.encomenda,
        data_solicitacao: req.body.data_solicitacao,
        nova_quantidade: req.body.nova_quantidade,
        nova_data_conclusao: req.body.nova_data_conclusao,
        cancelar_encomenda: req.body.cancelar_encomenda,
        produto: req.body.produto,
        cliente: req.body.cliente
    });
    return solicitacao;
}

solicitacaoSchemaToDTO = function (result) {
    const encDTO = new SolicitacaoDTO({
        _id: result[0]._id,
        encomenda: result[0].encomenda,
        data_solicitacao: result[0].data_solicitacao,
        nova_quantidade: result[0].nova_quantidade,
        nova_data_conclusao: result[0].nova_data_conclusao,
        cancelar_encomenda: result[0].cancelar_encomenda,
        produto: result[0].produto,
        cliente: result[0].cliente
        
    });
    return encDTO;
}

allSolicitacaoSchemaToDTO = function (result) {
    var i = 0;
    const array = [];
    for (i; i < result.length; i++) {
        const encDTO = new SolicitacaoDTO({
            _id: result[i]._id,
            encomenda: result[i].encomenda,
            data_solicitacao: result[i].data_solicitacao,
            nova_quantidade: result[i].nova_quantidade,
            nova_data_conclusao: result[i].nova_data_conclusao,
            cancelar_encomenda: result[i].cancelar_encomenda,
            produto: result[i].produto,
            cliente: result[i].cliente
        });
        array.push(encDTO);
    }
    return array;
}

module.exports = { solicitacaoJsonToSchema, allSolicitacaoSchemaToDTO, solicitacaoSchemaToDTO }