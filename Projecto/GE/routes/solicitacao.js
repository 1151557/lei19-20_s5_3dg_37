const express = require('express');
const router = express.Router();

const solicitacaoController = require('../controller/solicitacao');

/*POST solicitacao*/
//localhost:3000/solicitacao/solicitacao/client
router.post('/', solicitacaoController.solicitacao_create);

/* GET all solicitacao. */
//localhost:3000/solicitacao/
router.get('/', solicitacaoController.solicitacao_get_all);

/* GET all solicitacao from encomenda. */
//localhost:3000/solicitacao/solicitacao/{encomendaid}
router.get('/solicitacao/:encomenda', solicitacaoController.solicitacao_get_byEncomenda);

/* GET solicitacao by ID. */
//localhost:3000/solicitacao/{id}
router.get('/:id', solicitacaoController.solicitacao_get_byID);

/* Delete solicitacao by ID. */
//localhost:3000/solicitacao/{id}
router.delete('/:id', solicitacaoController.solicitacao_delete_byID);

/* Delete solicitacao by encomendaID. */
//localhost:3000/solicitacao/solicitacao/{id}
router.delete('/encomenda/:id', solicitacaoController.solicitacao_delete_byEncomenda);

module.exports = router;