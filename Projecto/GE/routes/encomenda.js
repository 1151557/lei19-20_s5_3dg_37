const express = require('express');
const router = express.Router();

const encomendaController = require('../controller/encomenda');

/*POST encomenda*/
//localhost:3000/encomendas/encomenda/client
router.post('/encomenda/', encomendaController.encomendas_create);

/*PUT encomenda*/
//localhost:3000/encomendas/{id}
router.put('/:id', encomendaController.encomendas_update);

/* GET all encomendas. */
//localhost:3000/encomendas/
router.get('/', encomendaController.encomendas_get_all);

/* GET all encomendas quantidade de produto. */
//localhost:3000/encomendas/produto
router.get('/produto', encomendaController.encomendas_get_all_produto_quantidade);

/* GET numero encomendas produto */
//localhost:3000/encomendas/nprodutos
router.get('/nprodutos', encomendaController.encomendas_numero_encomendas_produto);

/* GET all encomendas from client. */
//localhost:3000/encomendas/encomenda/{Client}
router.get('/encomenda/:cliente', encomendaController.encomendas_get_byClient);

/* GET encomenda by ID. */
//localhost:3000/encomendas/{id}
router.get('/:id', encomendaController.encomendas_get_byID);

/* Delete encomenda by ID. */
//localhost:3000/encomendas/{id}
router.delete('/:id', encomendaController.encomendas_delete_byID);

/* Delete encomenda by user id. */
//localhost:3000/encomendas/deletebycliente/{id}
router.delete('/deletebycliente/:id', encomendaController.encomendas_delete_byCliente);

module.exports = router;