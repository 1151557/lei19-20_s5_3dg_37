const express = require('express');
const router = express.Router();
const clienteController = require('../controller/cliente');

//https://localhost:3001/clientes/signup
router.post('/signup', clienteController.signUpCliente);

//https://localhost:3001/clientes/login
router.post('/login', clienteController.loginCliente);

//https://localhost:3001/clientes/{id}
router.get('/:id', clienteController.consultarCliente);

//https://localhost:3001/clientes/
router.get('/', clienteController.consultarAllClientes);

//https://localhost:3001/clientes/{id}
router.put('/:id', clienteController.alterarCliente);

//https://localhost:3001/clientes/{id}
router.delete('/:id', clienteController.apagarCliente);

module.exports = router;
