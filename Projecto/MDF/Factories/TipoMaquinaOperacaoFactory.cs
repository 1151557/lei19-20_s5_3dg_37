using System;
using MDF.Model;
using System.Collections.Generic;
using System.Linq;
namespace MDF.Factories
{

    class TipoMaquinaOperacaoFactory
    {
        private readonly MDFContext _context;

        public TipoMaquinaOperacaoFactory(MDFContext context)
        {
            _context = context;
        }

        public List<TipoMaquinaOperacao> newTipoMaquinaOperacao(TipoMaquina tm, TipoMaquinaDTO tmdto)
        {
            List<TipoMaquinaOperacao> ret = new List<TipoMaquinaOperacao>();
            if (tmdto.Operacoes != null)
            {
                foreach (string abanador in tmdto.Operacoes)
                {
                    TipoMaquinaOperacao tmo = new TipoMaquinaOperacao();
                    tmo.TipoMaquinaId = tm.Id;
                    tmo.TipoMaquina = tm;
                    Guid guid = new Guid(abanador);
                    Operacao opr = _context.Operacoes.Where(oprc => oprc.Id == guid).FirstOrDefault();
                    if (opr != null)
                    {
                        tmo.Operacao = opr;
                        tmo.OperacaoId = guid;
                        ret.Add(tmo);
                    }
                }
            }
            return ret;
        }
    }
}