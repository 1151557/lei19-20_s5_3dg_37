using System;
using MDF.Model;
using System.Linq;
namespace MDF.Factories
{

    class MaquinaFactory
    {

        private readonly MDFContext _context;


        public MaquinaFactory(MDFContext context)
        {
            _context = context;
        }

        public Maquina newMaquina(MaquinaDTO maquinaDTO)
        {
            Maquina m = new Maquina();
            Guid idTipoMaquina = new Guid(maquinaDTO.Tipo);
            TipoMaquina tm = _context.TipoMaquina.Single(tmp => tmp.Id == idTipoMaquina);
            m.Nome = maquinaDTO.Nome;
            m.Modelo = maquinaDTO.Modelo;
            m.Marca = maquinaDTO.Marca;
            m.Tipo = tm;
            m.Localizacao = maquinaDTO.Localizacao;
            m.Id = Guid.NewGuid();
            m.Ativa = true;
            return m;
        }
    }
}