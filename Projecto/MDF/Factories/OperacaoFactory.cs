using MDF.Model;

namespace MDF.Factories
{
    class OperacaoFactory
    {
        public OperacaoFactory()
        {

        }

        public Operacao newOperacao(OperacaoDTO opDTO)
        {
            Operacao op = new Operacao(opDTO.Nome);
            op.Nome = opDTO.Nome;
            return op;
        }
    }
}