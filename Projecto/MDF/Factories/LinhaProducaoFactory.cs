using System;
using MDF.Model;
using System.Collections.Generic;
using System.Linq;
namespace MDF.Factories
{
    class LinhaProducaoFactory
    {
        private readonly MDFContext _context;
        public LinhaProducaoFactory(MDFContext context)
        {
            _context = context;
        }

        public LinhaProducao newLinhaProducao(LinhaProducaoDTO lpDTO)
        {
            LinhaProducao lp = new LinhaProducao();
            lp.Nome = lpDTO.Nome;
            lp.Id = Guid.NewGuid();
            lp.Eixo_X = lpDTO.Eixo_X;
            lp.Eixo_Y = lpDTO.Eixo_Y;
            ICollection<Maquina> Maquinas = new List<Maquina>();
            foreach (string str in lpDTO.ListaMaquinas)
            {
                Guid id = new Guid(str);
                Maquina m = _context.Maquinas.Single(maq => maq.Id == id);
                if (m != null)
                {
                    Maquinas.Add(m);
                }
            }
            lp.ListaMaquinas = Maquinas;
            return lp;
        }
    }
}