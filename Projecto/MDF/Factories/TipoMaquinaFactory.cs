using System;
using MDF.Model;
using System.Collections.Generic;
using System.Linq;
namespace MDF.Factories
{

    class TipoMaquinaFactory
    {
        private readonly MDFContext _context;

        public TipoMaquinaFactory(MDFContext context)
        {
            _context = context;
        }

        public TipoMaquina Update(TipoMaquinaDTO dto)
        {
            TipoMaquina tm = new TipoMaquina();
            if (dto.Operacoes != null)
            {
                tm = newTipoMaquina(dto);
            }
            else
            {
                tm.Tipo = dto.Tipo;
            }
            return tm;
        }

        public TipoMaquina newTipoMaquina(TipoMaquinaDTO dto)
        {
            TipoMaquina tm = new TipoMaquina();
            tm.Id = Guid.NewGuid();
            tm.Tipo = dto.Tipo;
            List<Operacao> list = new List<Operacao>();
            return tm;
        }
    }
}