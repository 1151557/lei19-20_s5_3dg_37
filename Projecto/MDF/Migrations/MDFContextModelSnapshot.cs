﻿// <auto-generated />
using System;
using MDF.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace MDF.Migrations
{
    [DbContext(typeof(MDFContext))]
    partial class MDFContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("MDF.Model.LinhaProducao", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Axis");

                    b.Property<string>("Nome")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("Nome")
                        .IsUnique();

                    b.ToTable("LinhasProducao");
                });

            modelBuilder.Entity("MDF.Model.Maquina", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid?>("LinhaProducaoId");

                    b.Property<string>("Localizacao")
                        .IsRequired();

                    b.Property<string>("Marca")
                        .IsRequired();

                    b.Property<string>("Modelo")
                        .IsRequired();

                    b.Property<string>("Nome")
                        .IsRequired();

                    b.Property<Guid>("TipoId");

                    b.HasKey("Id");

                    b.HasIndex("LinhaProducaoId");

                    b.HasIndex("Nome")
                        .IsUnique();

                    b.HasIndex("TipoId");

                    b.ToTable("Maquinas");
                });

            modelBuilder.Entity("MDF.Model.Operacao", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Nome")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("Nome")
                        .IsUnique();

                    b.ToTable("Operacoes");
                });

            modelBuilder.Entity("MDF.Model.TipoMaquina", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Tipo")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("Tipo")
                        .IsUnique();

                    b.ToTable("TipoMaquina");
                });

            modelBuilder.Entity("MDF.Model.TipoMaquinaOperacao", b =>
                {
                    b.Property<Guid>("TipoMaquinaId");

                    b.Property<Guid>("OperacaoId");

                    b.HasKey("TipoMaquinaId", "OperacaoId");

                    b.HasIndex("OperacaoId");

                    b.ToTable("TipoMaquinaOperacao");
                });

            modelBuilder.Entity("MDF.Model.Maquina", b =>
                {
                    b.HasOne("MDF.Model.LinhaProducao")
                        .WithMany("ListaMaquinas")
                        .HasForeignKey("LinhaProducaoId");

                    b.HasOne("MDF.Model.TipoMaquina", "Tipo")
                        .WithMany()
                        .HasForeignKey("TipoId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("MDF.Model.TipoMaquinaOperacao", b =>
                {
                    b.HasOne("MDF.Model.Operacao", "Operacao")
                        .WithMany()
                        .HasForeignKey("OperacaoId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("MDF.Model.TipoMaquina", "TipoMaquina")
                        .WithMany("TipoMaquinaOperacao")
                        .HasForeignKey("TipoMaquinaId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
