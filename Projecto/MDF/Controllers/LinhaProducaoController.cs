using Microsoft.AspNetCore.Mvc;
using MDF.Model;
using MDF.Factories;
using MDF.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
namespace MDF.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LinhaProducaoController : ControllerBase
    {
        private readonly MDFContext _context;

        private LinhaProducaoFactory factory;

        private LinhaProducaoRepository repository;

        private MaquinaRepository maquinaRepository;

        public LinhaProducaoController(MDFContext context)
        {
            _context = context;
            factory = new LinhaProducaoFactory(context);
            repository = new LinhaProducaoRepository(context);
            maquinaRepository = new MaquinaRepository(context);
        }

        [HttpGet("{id}", Name = "GetLinhaProducao")]
        public ActionResult<LinhaProducao> GetById(Guid id)
        {
            LinhaProducao lp = repository.GetById(id);
            if (lp == null)
            {
                return NotFound();
            }

            return lp;
        }

        [HttpGet(Name = "GetLinhaProducaoByNome")]
        [Route("[action]/{id}")]
        public ActionResult<LinhaProducao> GetLinhaProducaoByNome(String id)
        {
            LinhaProducao linhaProducao = repository.GetByNome(id);

            if (linhaProducao == null)
            {
                return NotFound();
            }

            return linhaProducao;

        }
        [Route("~/api/GetAllLinhasProducao")]
        [HttpGet(Name = "GetAllLinhasProducao")]
        public IQueryable<LinhaProducao> GetAll()
        {

            List<LinhaProducao> listLinhasProducao = repository.GetAll().ToList();

            return listLinhasProducao.AsQueryable();
        }

        [HttpPost]
        public IActionResult Create(LinhaProducaoDTO linhaProducao)
        {
            if (linhaProducao.Eixo_X > 45 || linhaProducao.Eixo_X < -45
            || linhaProducao.Eixo_Y > 30 || linhaProducao.Eixo_Y < -30 || linhaProducao.Eixo_Y + (linhaProducao.ListaMaquinas.Count() * 7) > 30)
            {
                return BadRequest();
            }
            LinhaProducao lp = factory.newLinhaProducao(linhaProducao);
            lp = repository.Create(lp);
            if (lp == null)
            {
                return BadRequest();
            }
            return CreatedAtRoute("GetLinhaProducao", new { id = lp.Id }, lp);
        }

        [HttpPut("{id}")]
        public IActionResult Update(Guid id, LinhaProducaoDTO lpdto)
        {
            if (lpdto.Eixo_X > 45 || lpdto.Eixo_X < -45
             || lpdto.Eixo_Y > 30 || lpdto.Eixo_Y < -30 || lpdto.Eixo_Y + (lpdto.ListaMaquinas.Count() * 7) > 30)
            {
                return BadRequest();
            }
            LinhaProducao lp = repository.GetById(id);
            lp.Eixo_X = lpdto.Eixo_X;
            lp.Eixo_Y = lpdto.Eixo_Y;
            ICollection<Maquina> ListaMaquinas = maquinaRepository.GetListById(lpdto.ListaMaquinas);
            if (ListaMaquinas.Any())
            {
                lp.ListaMaquinas = ListaMaquinas;
            }
            else
            {
                lp.ListaMaquinas = new List<Maquina>();
            }
            lp = repository.Update(id, lp);
            if (lp == null)
            {
                return NotFound();
            }
            return Ok(lp);
        }

        [Route("~/api/updatelp/{id}/{xx}/{yy}")]
        [HttpPut]
        public IActionResult UpdateAxis(Guid id, int xx, int yy)
        {
            LinhaProducao lp = repository.GetById(id);
            if (xx > 45 || xx < -45
            || yy > 30 || yy < -30 || yy + (lp.ListaMaquinas.Count() * 7) > 30)
            {
                return BadRequest();
            }
            lp.Eixo_X = xx;
            lp.Eixo_Y = yy;

            lp = repository.Update(id, lp);
            if (lp == null)
            {
                return NotFound();
            }
            return Ok(lp);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            LinhaProducao lp = repository.Delete(id);

            if (lp == null)
            {
                return NotFound();
            }
            return Ok();
        }
    }
}