using System;
using System.Collections.Generic;
using System.Linq;
using MDF.Factories;
using MDF.Model;
using MDF.Repository;
using Microsoft.AspNetCore.Mvc;

namespace MDF.Controllers
{

    [Route("api/[controller]")]
    [ApiController]

    public class MaquinaController : ControllerBase
    {

        private readonly MDFContext _context;

        private MaquinaRepository repository;

        private MaquinaFactory factory;

        public MaquinaController(MDFContext context)
        {

            _context = context;
            factory = new MaquinaFactory(context);
            repository = new MaquinaRepository(context);
        }

        [HttpGet("{id}", Name = "GetMaquina")]
        public ActionResult<Maquina> GetById(Guid id)
        {
            Maquina maquina = repository.GetById(id);

            if (maquina == null)
            {
                return NotFound();
            }

            return maquina;
        }

        [HttpGet(Name = "GetMaquinaByNome")]
        [Route("[action]/{id}")]
        public ActionResult<Maquina> GetMaquinaByNome(String id)
        {
            Maquina maquina = repository.GetByNome(id);

             if (maquina == null)
            {
                return NotFound();
            }

            return maquina;

        }

        [HttpGet("{id}/GetMaquinasTipoMaquina")]
        public IQueryable<Maquina> GetByTipoId(Guid id)
        {
            List<Maquina> listMaquinaTipo = repository.GetByTipoId(id).ToList();

            return listMaquinaTipo.AsQueryable();
        }

        [Route("~/api/GetAllMaquinas")]
        [HttpGet(Name = "GetAllMaquinas")]

        public IQueryable<Maquina> GetAll()
        {

            List<Maquina> listMaquinas = repository.GetAll().ToList();

            return listMaquinas.AsQueryable();
        }

        [HttpPost]
        public IActionResult Create(MaquinaDTO maquinaDTO)
        {

            Maquina m = factory.newMaquina(maquinaDTO);
            repository.Create(m);
            return CreatedAtRoute("GetMaquina", new { id = m.Id }, m);

        }

        [HttpPut("{id}/{tipomaquina}")]
        public IActionResult Update(Guid id, string tipomaquina)
        {
            Maquina u = repository.Update(id, tipomaquina);
            if (u == null)
            {
                return NotFound();
            }
            return Ok(u);
        }
        
        [Route("~/api/activity/{id}/{activity}")]
        [HttpPut]
        public IActionResult UpdateActivity(Guid id, string activity)
        {
            Maquina u = repository.UpdateActivity(id, activity);
            if (u == null)
            {
                return NotFound();
            }
            return Ok(u);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            Maquina u = repository.Delete(id);

            if (u == null)
            {
                return NotFound();
            }
            return Ok();
        }

    }

}