using Microsoft.AspNetCore.Mvc;
using MDF.Model;
using MDF.Factories;
using MDF.Repository;
using System;
using System.Linq;
using System.Collections.Generic;
namespace MDF.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OperacaoController : ControllerBase
    {
        private readonly MDFContext _context;

        private OperacaoFactory factory;

        private OperacaoRepository repository;
        public OperacaoController(MDFContext context)
        {
            _context = context;
            factory = new OperacaoFactory();
            repository = new OperacaoRepository(context);
        }

        [HttpGet(Name = "GetOperacao")]
        [Route("[action]/{id}")]
        public ActionResult<Operacao> GetById(Guid id)
        {
            Operacao op = repository.GetById(id);
            if (op == null)
            {
                return NotFound();
            }

            return op;
        }

        [HttpGet(Name = "GetOperacaoByNome")]
        [Route("[action]/{id}")]
        public ActionResult<Operacao> GetByNome(String id)
        {
            Operacao op = repository.GetByNome(id);
            if (op == null)
            {
                return NotFound();
            }

            return op;
        }

        [HttpGet(Name = "GetAllOperacoes")]
        [Route("~/api/GetAllOperacoes")]
        public IQueryable<Operacao> GetAll()
        {

            List<Operacao> listOperacoes = repository.GetAll().ToList();

            return listOperacoes.AsQueryable();
        }


        [HttpPost]
        public IActionResult Create(OperacaoDTO operacao)
        {
            Operacao op = factory.newOperacao(operacao);
            repository.Create(op);
            return CreatedAtRoute("GetOperacao", new { id = op.Id }, op);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            Operacao op = repository.Delete(id);

            if (op == null)
            {
                return NotFound();
            }
            return Ok();
        }
    }
}