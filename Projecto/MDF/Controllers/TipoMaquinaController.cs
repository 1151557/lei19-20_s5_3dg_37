using MDF.Model;
using MDF.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using MDF.Factories;
using System.Collections.Generic;
namespace MDF.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class TipoMaquinaController : ControllerBase
    {

        private readonly MDFContext _context;

        private TipoMaquinaRepository repository;

        private TipoMaquinaFactory factory;

        private TipoMaquinaOperacaoFactory factoryitm;

        public TipoMaquinaController(MDFContext context)
        {
            _context = context;
            factory = new TipoMaquinaFactory(context);
            factoryitm = new TipoMaquinaOperacaoFactory(context);
            repository = new TipoMaquinaRepository(context);
        }

        [HttpGet("GetAllTipoMaquinas")]
        public IQueryable<TipoMaquina> GetAll()
        {
            List<TipoMaquina> list = repository.GetAll().ToList();

            return list.AsQueryable();
        }

        [HttpGet("{id}", Name = "GetTipoMaquina")]
        public ActionResult<TipoMaquina> GetById(Guid id)
        {
            TipoMaquina tm = repository.GetById(id);
            if (tm == null)
            {
                return NotFound();
            }

            return tm;
        }

        [HttpGet(Name = "GetTipoMaquinaByNome")]
        [Route("[action]/{id}")]
        public ActionResult<TipoMaquina> GetTipoMaquinaByNome(String id)
        {
            TipoMaquina tipoMaquina = repository.GetByNome(id);

            if (tipoMaquina == null)
            {
                return NotFound();
            }

            return tipoMaquina;

        }

        [HttpGet("{id}/GetOperacoes")]
        public IActionResult GetOperacoestById(Guid id)
        {
            List<Operacao> ops = repository.GetOperacoesById(id);

            if (ops == null)
            {
                return NotFound();
            }

            return Ok(ops);
        }

        [HttpPost]
        public IActionResult Create(TipoMaquinaDTO dto)
        {
            TipoMaquina tm = factory.newTipoMaquina(dto);
            List<TipoMaquinaOperacao> tmo = factoryitm.newTipoMaquinaOperacao(tm, dto);
            tm.TipoMaquinaOperacao = tmo;
            repository.Create(tm);
            return CreatedAtRoute("GetTipoMaquina", new { id = tm.Id }, tm);
        }

        [HttpPut("{action}/{id}")]
        public IActionResult UpdateOperacoesMaquina(Guid id, TipoMaquinaDTO dto)
        {
            TipoMaquina tm = repository.GetById(id);
            List<TipoMaquinaOperacao> tmo = factoryitm.newTipoMaquinaOperacao(tm, dto);
            tm.TipoMaquinaOperacao = tmo;
            TipoMaquina u = repository.Update(id, tm);

            if (u == null)
            {
                return NotFound();
            }

            return Ok(u);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            TipoMaquina u = repository.Delete(id);

            if (u == null)
            {
                return NotFound();
            }
            return Ok();
        }

    }
}

