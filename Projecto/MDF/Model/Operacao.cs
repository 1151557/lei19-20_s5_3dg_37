using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace MDF.Model
{
    public class Operacao
    {
        public Guid Id { get; set; }

        [Required]
        public string Nome { get; set; }


        public Operacao(string nome)
        {
            Id = Guid.NewGuid();
            Nome = nome;
        }
    }
}