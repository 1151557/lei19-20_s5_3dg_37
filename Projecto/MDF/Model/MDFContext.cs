using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace MDF.Model
{
    public class MDFContext : DbContext
    {

        public MDFContext(DbContextOptions<MDFContext> options) : base(options)
        {

        }
        public DbSet<Operacao> Operacoes { get; set; }

        public DbSet<Maquina> Maquinas { get; set; }

        public DbSet<TipoMaquina> TipoMaquina { get; set; }

        public DbSet<LinhaProducao> LinhasProducao { get; set; }

        public DbSet<TipoMaquinaOperacao> TipoMaquinaOperacao { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TipoMaquinaOperacao>().HasKey(tmo => new { tmo.TipoMaquinaId, tmo.OperacaoId });
            //modelBuilder.Entity<TipoMaquinaOperacao>().HasOne(tmo => tmo.Operacao).WithMany(op => op.TipoMaquinaOperacao).HasForeignKey(tmo => tmo.OperacaoId);
            modelBuilder.Entity<TipoMaquinaOperacao>().HasOne(tmo => tmo.TipoMaquina).WithMany(tm => tm.TipoMaquinaOperacao).HasForeignKey(tmo => tmo.TipoMaquinaId);
            modelBuilder.Entity<Operacao>().HasIndex(op => op.Nome).IsUnique();
            modelBuilder.Entity<Maquina>().HasIndex(m => m.Nome).IsUnique();
            modelBuilder.Entity<TipoMaquina>().HasIndex(tp => tp.Tipo).IsUnique();
            modelBuilder.Entity<LinhaProducao>().HasIndex(lp => lp.Nome).IsUnique();
        }
    }
}