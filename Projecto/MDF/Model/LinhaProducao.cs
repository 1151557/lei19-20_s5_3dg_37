using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MDF.Model
{
    public class LinhaProducao
    {

        public Guid Id { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public ICollection<Maquina> ListaMaquinas { get; set; }
        public int Eixo_X { get; set; }
        public int Eixo_Y { get; set; }

        public int getTamanho()
        {
            if (this.ListaMaquinas == null)
            {
                return 0;
            }
            else if (this.ListaMaquinas.Count % 2 == 0)
            {
                return this.ListaMaquinas.Count * 7;
            }
            return (this.ListaMaquinas.Count + 1) * 7;
        }
    }
}