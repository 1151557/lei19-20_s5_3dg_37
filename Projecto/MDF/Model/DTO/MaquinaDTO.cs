using System;
using System.Collections.Generic;

namespace MDF.Model {
    
    public class MaquinaDTO {

        public string Nome {get; set;}

        public Guid Id {get; set;}

        public string Tipo {get; set;}

        public string Marca {get; set;}

        public string Modelo {get; set;}

        public string Localizacao {get; set;}

        public Boolean Ativa {get; set;}

    }   

}