using System;
using System.Collections.Generic;

namespace MDF.Model
{
    public class LinhaProducaoDTO
    {
        public string Nome { get; set; }
        public ICollection<String> ListaMaquinas { get; set; }
        public int Eixo_X { get; set; }
        public int Eixo_Y { get; set; }
    }
}