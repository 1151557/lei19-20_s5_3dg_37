using System;
using System.Collections.Generic;

namespace MDF.Model
{
    public class TipoMaquinaDTO
    {

        public string Tipo { get; set; }

        public List<string> Operacoes { get; set; }

    }
}