using System;

namespace MDF.Model
{
    public class TipoMaquinaOperacao
    {
        //public Guid Id { get; set; }
        public Guid TipoMaquinaId { get; set; }
        public TipoMaquina TipoMaquina { get; set; }
        public Guid OperacaoId { get; set; }
        public Operacao Operacao { get; set; }
    }
}