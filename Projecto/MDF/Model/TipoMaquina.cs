using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MDF.Model
{
    public class TipoMaquina
    {
        public Guid Id { get; set; }

        [Required]
        public string Tipo { get; set; }

        public ICollection<TipoMaquinaOperacao> TipoMaquinaOperacao { get; set; }

    }
}