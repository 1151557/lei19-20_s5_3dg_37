using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;
namespace MDF.Model
{
    public class Maquina
    {

        [Required]
        public string Nome { get; set; }

        public Guid Id { get; set; }

        [Required]

        public TipoMaquina Tipo { get; set; }

        [Required]
            public string Marca {  get; set; }

        [Required]
        public string Modelo { get; set; }

        [Required]
        public string Localizacao { get; set; }

        public Boolean Ativa{ get; set;}

    }
}