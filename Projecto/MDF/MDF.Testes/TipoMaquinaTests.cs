using System;
using Xunit;
using System.Collections.Generic;
using MDF.Model;

namespace MDF.Testes
{
    public class TipoMaquinaTests
    {
        [Fact]
        public void hasId()
        {
            TipoMaquina tp = new TipoMaquina()
            {
                Id = new Guid("2E96DDDA-DCF7-4804-BA1F-4464E82B8CBA")
            };
            Assert.True(tp.Id == new Guid("2E96DDDA-DCF7-4804-BA1F-4464E82B8CBA"));
        }

        [Fact]
        public void hasTipo()
        {
            TipoMaquina tp = new TipoMaquina()
            {
                Tipo = "Tipo Teste"
            };
            Assert.True(tp.Tipo == "Tipo Teste");
        }

        [Fact]
        public void HasNull(){
            TipoMaquina tp = new TipoMaquina();
            //Assert.Null(tp.Operacoes);
            Assert.Null(tp.Tipo);
        }

    }
}
