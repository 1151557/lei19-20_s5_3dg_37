using System;
using Xunit;
using MDF.Model;

namespace MDF.Testes
{
    public class OperacaoTests
    {
        [Fact]
        public void hasId()
        {
            Operacao o = new Operacao("landim");
            Assert.True(o.Id != null);
        }

        [Fact]
        public void hasNome()
        {
            Operacao o = new Operacao("cortar pao");
            Assert.True(o.Nome == "cortar pao");
        }
    }
}
