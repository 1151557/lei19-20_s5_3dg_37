using System;
using Xunit;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using MDF.Model;

namespace MDF.Testes
{
    public class LinhaProducaoTests
    {
        [Fact]
        public void hasId()
        {
            LinhaProducao o = new LinhaProducao()
            {
                Id = new Guid("2E96DDDA-DCF7-4804-BA1F-4464E82B8CBA")
            };
            Assert.True(o.Id == new Guid("2E96DDDA-DCF7-4804-BA1F-4464E82B8CBA"));
        }

        [Fact]
        public void hasNome()
        {
            LinhaProducao o = new LinhaProducao()
            {
                Nome = "fazer pao"
            };
            Assert.True(o.Nome == "fazer pao");
        }

        [Fact]
        public void hasOperacoes()
        {
            Maquina m = new Maquina();
            ICollection<Maquina> maq = new Collection<Maquina>();
            maq.Add(m);
            LinhaProducao lp = new LinhaProducao()
            {
                ListaMaquinas = maq
            };
            Assert.True(lp.ListaMaquinas == maq);
        }
    }
}
