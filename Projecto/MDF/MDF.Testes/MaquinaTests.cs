using System;
using Xunit;
using MDF.Model;

namespace MDF.Testes 
{
    public class MaquinaTests
    {
        [Fact]
        public void hasId()
        {
        
            Maquina maquina = new Maquina()
            {
                Id = new Guid("2E96DDDA-DCF7-4804-BA1F-4464E82B8CBA")

            };
            Assert.True(maquina.Id == new Guid("2E96DDDA-DCF7-4804-BA1F-4464E82B8CBA"));
        }

        [Fact]
        public void hasNome() 
        {
            Maquina maquina = new Maquina()
            {
                Nome = "Maquina nome"
            };
            Assert.True(maquina.Nome == "Maquina nome");
        }

        [Fact]
        public void hasTipo()
        {
            TipoMaquina tipoMaquina = new TipoMaquina();
            
            Maquina maquina = new Maquina()
            {
                Tipo = tipoMaquina
            };
            Assert.True(maquina.Tipo == tipoMaquina);
        }

        [Fact]
        public void hasMarca()
        {
            Maquina maquina = new Maquina()
            {
                Marca = "Maquina marca"
            };
            Assert.True(maquina.Marca == "Maquina marca");
        }

        [Fact]
        public void hasModelo()
        {
            Maquina maquina = new Maquina()
            {
                Modelo = "Maquina modelo"
            };
            Assert.True(maquina.Modelo == "Maquina modelo");
        }

        [Fact]
        public void hasLocalizacao()
        {
            Maquina maquina = new Maquina()
            {
                Localizacao = "Maquina localizacao"
            };
            Assert.True(maquina.Localizacao == "Maquina localizacao");
        }
    }
}