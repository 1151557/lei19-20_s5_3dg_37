using System;
using System.Linq;
using MDF.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace MDF.Repository
{

    public class TipoMaquinaRepository : IRepository<TipoMaquina>
    {

        private readonly MDFContext _context;

        public TipoMaquinaRepository()
        {

        }
        public TipoMaquinaRepository(MDFContext context)
        {
            _context = context;
        }

        public TipoMaquina Create(TipoMaquina entity)
        {
            _context.TipoMaquina.Add(entity);
            _context.SaveChanges();

            return entity;
        }

        public TipoMaquina Delete(Guid id)
        {

            TipoMaquina tipoMaquina = _context.TipoMaquina.Find(id);

            _context.TipoMaquina.Remove(tipoMaquina);
            _context.SaveChanges();

            return tipoMaquina;
        }

        public IQueryable<TipoMaquina> GetAll()
        {

            IQueryable<TipoMaquina> tipoMaquina = _context.TipoMaquina.Include("TipoMaquinaOperacao.Operacao");

            return tipoMaquina;
        }

        public TipoMaquina GetByNome(string tipo)
        {

            TipoMaquina tipoMaquina = _context.TipoMaquina.Include("TipoMaquinaOperacao.Operacao").Where(m => m.Tipo.Equals(tipo)).FirstOrDefault();

            return tipoMaquina;
        }

        public TipoMaquina GetById(Guid id)
        {

            TipoMaquina tipoMaquina = _context.TipoMaquina.Include("TipoMaquinaOperacao.Operacao").Where(m => m.Id == id).FirstOrDefault();
            return tipoMaquina;

        }

        public List<Operacao> GetOperacoesById(Guid id)
        {
            List<TipoMaquinaOperacao> tmo = _context.TipoMaquina.Include("TipoMaquinaOperacao.Operacao").Where(m => m.Id == id).FirstOrDefault().TipoMaquinaOperacao.ToList();
            List<Operacao> ret = new List<Operacao>();
            foreach (TipoMaquinaOperacao abanador in tmo)
            {
                ret.Add(abanador.Operacao);
            }
            return ret;
        }

        public TipoMaquina Update(Guid id, TipoMaquina entity)
        {
            _context.TipoMaquina.Update(entity);
            _context.SaveChanges();
            return _context.TipoMaquina.Include("TipoMaquinaOperacao.Operacao").Where(m => m.Id.Equals(id)).FirstOrDefault();
        }

    }
}