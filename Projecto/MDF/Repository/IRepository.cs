using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace MDF.Repository
{
    public interface IRepository<T>{

        IQueryable<T> GetAll();
        T GetById(Guid id);
        T Create(T entity);
        T Update(Guid id, T entity);
        T Delete(Guid id);
    }
}