using System;
using System.Collections.Generic;
using System.Linq;
using MDF.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
namespace MDF.Repository
{

    public class MaquinaRepository
    {

        public readonly MDFContext _context;
        public MaquinaRepository(MDFContext context)
        {

            _context = context;

        }

        public Maquina Create(Maquina entity)
        {

            _context.Maquinas.Add(entity);
            _context.SaveChanges();

            return entity;

        }

        public Maquina Delete(Guid id)
        {

            Maquina maquina = _context.Maquinas.Find(id);

            _context.Maquinas.Remove(maquina);
            _context.SaveChanges();

            return maquina;
        }

        public IQueryable<Maquina> GetAll()
        {

            IQueryable<Maquina> maquina = _context.Maquinas.Include("Tipo.TipoMaquinaOperacao.Operacao");
            return maquina;
        }

        public ICollection<Maquina> GetListById(ICollection<String> listaMaquinas)
        {
            ICollection<Maquina> ret = new List<Maquina>();
            foreach (string s in listaMaquinas)
            {
                Maquina maquina = GetById(new Guid(s));
                if (maquina != null)
                {
                    ret.Add(maquina);
                }
            }
            return ret;
        }

        public Maquina GetById(Guid id)
        {

            Maquina maquina = _context.Maquinas.Include("Tipo.TipoMaquinaOperacao.Operacao").Where(m => m.Id == id).FirstOrDefault();

            return maquina;

        }

        public Maquina GetByNome(String name)
        {
            Maquina maquina = _context.Maquinas.Include("Tipo.TipoMaquinaOperacao.Operacao").Where(m => m.Nome.Equals(name)).FirstOrDefault();

            return maquina;
        }

        public IQueryable<Maquina> GetByTipoId(Guid id)
        {

            IQueryable<Maquina> maquina = _context.Maquinas.Include("Tipo.TipoMaquinaOperacao.Operacao").Where(m => m.Tipo.Id == id);

            return maquina;
        }

        public Maquina Update(Guid id, String tipomaquina)
        {

            var maquina = _context.Maquinas.Include("Tipo.TipoMaquinaOperacao.Operacao").Where(m => m.Id == id).FirstOrDefault();
            Guid tipomaquinaId = new Guid(tipomaquina);
            TipoMaquina tp = _context.TipoMaquina.Where(tmp => tmp.Id.Equals(tipomaquinaId)).FirstOrDefault();
            if (tp != null)
            {
                maquina.Tipo = tp;
                _context.Maquinas.Update(maquina);
                _context.SaveChanges();
            }
            return maquina;
        }

        public Maquina UpdateActivity(Guid id, String ativa)
        {

            var maquina = _context.Maquinas.Include("Tipo.TipoMaquinaOperacao.Operacao").Where(m => m.Id == id).FirstOrDefault();
            if (ativa.Equals("false"))
            {
                maquina.Ativa = false;
                _context.Maquinas.Update(maquina);
                _context.SaveChanges();
            }else if(ativa.Equals("true")){
                maquina.Ativa = true;
                _context.Maquinas.Update(maquina);
                _context.SaveChanges();
            }
            return maquina;
        }

    }
}