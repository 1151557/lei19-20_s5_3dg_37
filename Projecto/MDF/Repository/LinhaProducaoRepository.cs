using System;
using MDF.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace MDF.Repository
{
    public class LinhaProducaoRepository : IRepository<LinhaProducao>
    {
        private readonly MDFContext _context;

        public LinhaProducaoRepository(MDFContext context)
        {
            _context = context;
        }

        public LinhaProducao Create(LinhaProducao entity)
        {
            List<LinhaProducao> lplist = _context.LinhasProducao.Where(lpd => lpd.Eixo_X >= entity.Eixo_X - 6.9 && lpd.Eixo_X <= entity.Eixo_X + 6.9).ToList();
            foreach (LinhaProducao lp in lplist)
            {
                if (lp.Eixo_Y + lp.getTamanho() > entity.Eixo_Y && lp.Eixo_Y < entity.Eixo_Y + entity.getTamanho())
                {
                    return null;
                }
            }
            _context.LinhasProducao.Add(entity);
            _context.SaveChanges();
            return entity;
        }

        public LinhaProducao Delete(Guid id)
        {
            LinhaProducao lp = _context.LinhasProducao.Find(id);

            _context.LinhasProducao.Remove(lp);
            _context.SaveChanges();
            return lp;
        }

        public IQueryable<LinhaProducao> GetAll()
        {
            IQueryable<LinhaProducao> lp = _context.LinhasProducao.Include("ListaMaquinas.Tipo.TipoMaquinaOperacao.Operacao");

            return lp;
        }

        public LinhaProducao GetById(Guid id)
        {
            LinhaProducao lp = _context.LinhasProducao
            .Include("ListaMaquinas")
            .Where(lpd => lpd.Id == id)
            .FirstOrDefault();
            return lp;
        }

        public LinhaProducao GetByNome(String nome)
        {
            LinhaProducao linhaProducao = _context.LinhasProducao.Include("ListaMaquinas.Tipo.TipoMaquinaOperacao.Operacao").Where(lpd => lpd.Nome.Equals(nome)).FirstOrDefault();

            return linhaProducao;
        }

        public LinhaProducao Update(Guid id, LinhaProducao entity)
        {
            List<LinhaProducao> lplist = _context.LinhasProducao.Where(lpd => lpd.Eixo_X >= entity.Eixo_X - 6.9 && lpd.Eixo_X <= entity.Eixo_X + 6.9).ToList();
            foreach (LinhaProducao lpi in lplist)
            {
                if (lpi.Eixo_Y + lpi.getTamanho() > entity.Eixo_Y && lpi.Eixo_Y < entity.Eixo_Y + entity.getTamanho())
                {
                    return null;
                }
            }
            var lp = _context.LinhasProducao.Find(id);
            lp.Nome = entity.Nome;
            lp.ListaMaquinas = entity.ListaMaquinas;
            lp.Eixo_X = entity.Eixo_X;
            lp.Eixo_Y = entity.Eixo_Y;
            _context.LinhasProducao.Update(lp);
            _context.SaveChanges();
            return lp;
        }
    }
}