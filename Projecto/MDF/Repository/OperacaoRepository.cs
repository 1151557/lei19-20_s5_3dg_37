using System;
using MDF.Model;
using System.Linq;

namespace MDF.Repository
{
    public class OperacaoRepository : IRepository<Operacao>
    {
        private readonly MDFContext _context;

        public OperacaoRepository(MDFContext context)
        {
            _context = context;
        }

        public Operacao Create(Operacao entity)
        {
            _context.Operacoes.Add(entity);
            _context.SaveChanges();

            return entity;
        }

        public Operacao Delete(Guid id)
        {
            Operacao op = _context.Operacoes.Find(id);

            _context.Operacoes.Remove(op);
            _context.SaveChanges();
            return op;
        }

        public IQueryable<Operacao> GetAll()
        {
            IQueryable<Operacao> op = _context.Operacoes;

            return op;
        }

        public Operacao GetById(Guid id)
        {
            Operacao op = _context.Operacoes
                .Where(opr => opr.Id == id)
                .FirstOrDefault();

            return op;
        }

        public Operacao GetByNome(String name)
        {
            Operacao op = _context.Operacoes
                .Where(opr => opr.Nome.Equals(name))
                .FirstOrDefault();

            return op;
        }

        public Operacao Update(Guid id, Operacao entity)
        {
            var op = _context.Operacoes.Find(id);
            op.Nome = entity.Nome;

            _context.Operacoes.Update(op);
            _context.SaveChanges();
            return op;
        }
    }
}