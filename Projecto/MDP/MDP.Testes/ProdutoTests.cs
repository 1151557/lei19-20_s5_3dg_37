using System;
using Xunit;
using MDP.Model;
using MDF.Model;
using System.Collections.Generic;

namespace MDP.Testes
{
    public class ProdutoTests
    {
        [Fact]
        public void hasId()
        {
            Produto p = new Produto()
            {
                Id = new Guid("2E96DDDA-DCF7-4804-BA1F-4464E82B8CBA")
            };
            Assert.True(p.Id == new Guid("2E96DDDA-DCF7-4804-BA1F-4464E82B8CBA"));
        }

        [Fact]
        public void hasNome()
        {
            Produto p = new Produto()
            {
                Nome = "Nome Teste"
            };
            Assert.True(p.Nome == "Nome Teste");
        }

        [Fact]
        public void hasPlanoFabrico()
        {
            Produto p = new Produto();
            List<Operacao> operacoes = new List<Operacao>();
            PlanoFabrico pf = new PlanoFabrico();
            p.PFabrico = pf;
            Assert.True(p.PFabrico == pf);
        }

        [Fact]
        public void HasNull()
        {
            Produto p = new Produto();
            Assert.Null(p.Nome);
            Assert.Null(p.PFabrico);
        }

    }
}
