using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using MDF.Model;
using MDP.Model;
using Xunit;

namespace MDP.Testes {
    public class PlanoFabricoTests {
        [Fact]
        public void hasId () 
        {

            PlanoFabrico planoFabrico = new PlanoFabrico () 
            {
                Id = new Guid ("df030488-6c2c-4226-bfba-1cc71645c0ca")

            };
            Assert.True (planoFabrico.Id == new Guid ("df030488-6c2c-4226-bfba-1cc71645c0ca"));
        }
    }
}