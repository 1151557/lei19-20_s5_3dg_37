using System;
using System.Linq;

namespace MDP.Repository
{
    public interface IRepository<T>{

        IQueryable<T> GetAll();
        T GetById(Guid id);
        T Create(T entity);
        T Update(Guid id, T entity);
        T Delete(Guid id);
    }
}