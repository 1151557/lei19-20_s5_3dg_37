using System;
using System.Linq;
using MDP.Model;
using Microsoft.EntityFrameworkCore;
using MDF.Model;

namespace MDP.Repository
{

    public class PlanoFabricoRepository : IRepository<PlanoFabrico>
    {

        public readonly MDPContext _context;

        public PlanoFabricoRepository()
        {

        }
        public PlanoFabricoRepository(MDPContext context)
        {

            _context = context;

        }

        public PlanoFabrico Create(PlanoFabrico entity)
        {
            _context.PlanosFabrico.Add(entity);
            _context.SaveChanges();
            return entity;
        }

        public PlanoFabrico Delete(Guid id)
        {

            PlanoFabrico planoFabrico = _context.PlanosFabrico.Find(id);

            _context.PlanosFabrico.Remove(planoFabrico);
            _context.SaveChanges();

            return planoFabrico;
        }

        public IQueryable<PlanoFabrico> GetAll()
        {

            IQueryable<PlanoFabrico> planoFabrico = _context.PlanosFabrico.Include("PlanoFabricoOperacao.Operacao");

            return planoFabrico;
        }

        public PlanoFabrico GetById(Guid id)
        {

            PlanoFabrico planoFabrico = _context.PlanosFabrico.Include("PlanoFabricoOperacao.Operacao").Where(pf => pf.Id == id).FirstOrDefault();

            return planoFabrico;

        }

        public PlanoFabrico Update(Guid id, PlanoFabrico entity)
        {
            _context.PlanosFabrico.Update(entity);
            _context.SaveChanges();
            return entity;
        }

    }
}