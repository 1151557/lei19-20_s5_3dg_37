using System;
using System.Linq;
using MDP.Model;
using Microsoft.EntityFrameworkCore;
using MDF.Model;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace MDP.Repository
{

    public class PlanoFabricoOperacaoRepository
    {
        private readonly MDPContext _context;

        public PlanoFabricoOperacaoRepository(MDPContext context)
        {
            _context = context;
        }
        public async Task<ICollection<Operacao>> getOperacoes(ICollection<string> idOperacoes)
        {
            List<Operacao> ret = new List<Operacao>();
            foreach (string s in idOperacoes)
            {
                var opi = _context.Operacoes.Find(new Guid(s));
                if (opi != null)
                {
                    ret.Add(opi);
                }
                else
                {
                    string sUrl = "https://mdf37.azurewebsites.net/api/operacao/getbyid/" + s;
                    using (HttpClient client = new HttpClient())
                    using (HttpResponseMessage res = await client.GetAsync(sUrl))
                    using (HttpContent content = res.Content)
                    {
                        string data = await content.ReadAsStringAsync();
                        if (data != null)
                        {
                            if ((int)res.StatusCode != 404)
                            {
                                Operacao op = JsonConvert.DeserializeObject<Operacao>(data);
                                ret.Add(op);
                            }
                        }
                    }
                }
            }
            return ret;
        }
    }
}