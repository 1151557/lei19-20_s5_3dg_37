using System;
using MDP.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace MDP.Repository
{

    public class ProdutoRepository : IRepository<Produto>
    {

        public readonly MDPContext _context;

        public ProdutoRepository()
        {

        }
        public ProdutoRepository(MDPContext context)
        {

            _context = context;

        }

        public Produto Create(Produto entity)
        {

            _context.Produtos.Add(entity);
            _context.SaveChanges();

            return entity;

        }

        public Produto Delete(Guid id)
        {

            Produto produto = _context.Produtos.Find(id);

            _context.Produtos.Remove(produto);
            _context.SaveChanges();

            return produto;
        }

        public IQueryable<Produto> GetAll()
        {

            IQueryable<Produto> produto = _context.Produtos.Include("PFabrico.PlanoFabricoOperacao.Operacao");

            return produto;
        }

        public Produto GetById(Guid id)
        {

            Produto produto = _context.Produtos.Include(pf => pf.PFabrico.PlanoFabricoOperacao).Where(p => p.Id == id).FirstOrDefault();

            return produto;

        }

        public Produto GetByNome(String nome) 
        {
            Produto produto = _context.Produtos.Include(pf => pf.PFabrico.PlanoFabricoOperacao).Where(p => p.Nome.Equals(nome)).FirstOrDefault();

            return produto;
        }

        public Produto Update(Guid id, Produto entity)
        {

            var produto = _context.Produtos.Find(id);
            produto.Nome = entity.Nome;
            produto.PFabrico = entity.PFabrico;

            _context.Produtos.Update(produto);
            _context.SaveChanges();

            return produto;

        }

        public Produto UpdateTempo(Guid id, string tempo)
        {

            var produto = _context.Produtos.Find(id);
            produto.TempoProducao = tempo;

            _context.Produtos.Update(produto);
            _context.SaveChanges();

            return produto;

        }

    }
}