using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MDP.Model
{
    public class PlanoFabrico
    {

        public Guid Id { get; set; }

        [Required]
        public string Nome { get; set; }

        public ICollection<PlanoFabricoOperacao> PlanoFabricoOperacao { get; set; }

    }
}