using System;
using System.ComponentModel.DataAnnotations;
namespace MDP.Model
{
    public class Produto
    {
        public Guid Id { get; set; }

        [Required]
        public string Nome { get; set; }

        [Required]
        public PlanoFabrico PFabrico { get; set; }

        public string TempoProducao { get; set; }

    }
}