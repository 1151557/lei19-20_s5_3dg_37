using System;
using MDF.Model;

namespace MDP.Model
{
    public class PlanoFabricoOperacao
    {
        //public Guid Id { get; set; }
        public Guid PlanoFabricoId { get; set; }
        public PlanoFabrico PlanoFabrico { get; set; }
        public Guid OperacaoId { get; set; }
        public Operacao Operacao { get; set; }
    }
}