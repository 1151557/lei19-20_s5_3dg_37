using System;
using System.Collections.Generic;
namespace MDP.Model
{
    public class PlanoFabricoDTO
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public ICollection<string> Operacoes { get; set; }

    }
}