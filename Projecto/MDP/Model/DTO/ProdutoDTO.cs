using System;
namespace MDP.Model
{
    public class ProdutoDTO
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }

        public string PFabrico { get; set; }

        public string TempoProducao { get; set; }

    }
}