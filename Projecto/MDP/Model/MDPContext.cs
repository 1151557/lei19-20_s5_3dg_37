using Microsoft.EntityFrameworkCore;
using MDF.Model;
using Microsoft.EntityFrameworkCore.Metadata;

namespace MDP.Model
{
    public class MDPContext : DbContext
    {
        public MDPContext(DbContextOptions<MDPContext> options) : base(options)
        {

        }

        public DbSet<PlanoFabrico> PlanosFabrico { get; set; }

        public DbSet<Produto> Produtos { get; set; }

        public DbSet<Operacao> Operacoes { get; set; }

        public DbSet<PlanoFabricoOperacao> PlanoFabricoOperacao { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PlanoFabricoOperacao>().HasKey(pfo => new { pfo.PlanoFabricoId, pfo.OperacaoId });
            modelBuilder.Entity<PlanoFabricoOperacao>().HasOne(pfo => pfo.PlanoFabrico).WithMany(tm => tm.PlanoFabricoOperacao).HasForeignKey(tmo => tmo.PlanoFabricoId);
            modelBuilder.Entity<Produto>().HasIndex(p => p.Nome).IsUnique();
            modelBuilder.Entity<Operacao>().HasIndex(op => op.Nome).IsUnique();
            modelBuilder.Entity<PlanoFabrico>().HasIndex(pf => pf.Nome).IsUnique();
        }
    }
}