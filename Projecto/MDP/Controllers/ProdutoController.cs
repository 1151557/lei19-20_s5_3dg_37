using Microsoft.AspNetCore.Mvc;
using MDP.Model;
using System.Linq;
using MDP.Factories;
using MDP.Repository;
using System.Collections.Generic;
using System;

namespace MDP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutoController : ControllerBase
    {
        private readonly MDPContext _context;

        private ProdutoFactory factory;

        private ProdutoRepository repository;
        public ProdutoController(MDPContext context)
        {
            _context = context;
            factory = new ProdutoFactory(context);
            repository = new ProdutoRepository(context);
        }

        [HttpGet("{id}", Name = "GetProduto")]
        public ActionResult<Produto> GetById(Guid id)
        {
            Produto p = repository.GetById(id);
            if (p == null)
            {
                return NotFound();
            }


            return p;
        }


        [HttpGet(Name = "GetProdutoByNome")]
        [Route("[action]/{id}")]
        public ActionResult<Produto> GetProdutoByNome(String id)
        {
            Produto produto = repository.GetByNome(id);

            if (produto == null)
            {
                return NotFound();
            }

            return produto;

        }

        [Route("~/api/GetAllProdutos")]
        [HttpGet(Name = "GetAllProdutos")]

        public IQueryable<Produto> GetAll()
        {

            List<Produto> listProdutos = repository.GetAll().ToList();
            foreach (var p in listProdutos)
            {
                p.PFabrico = factory.getProductoPlanoFabrico(p);
            }

            return listProdutos.AsQueryable();
        }

        [HttpGet("{id}/GetPlanoFabrico")]
        public PlanoFabrico GetPlanoFabrico(Guid id)
        {
            return repository.GetById(id).PFabrico;
        }

        [HttpPost]
        public IActionResult Create(ProdutoDTO produto)
        {
            Produto p = factory.newProduto(produto);
            repository.Create(p);
            return CreatedAtRoute("GetProduto", new { id = p.Id }, p);
        }

        [HttpPut("{id}")]
        public IActionResult Update(Guid id, ProdutoDTO produto)
        {
            Produto p = factory.newProduto(produto);
            p = repository.Update(id, p);

            if (p == null)
            {
                return NotFound();
            }

            return Ok(p);
        }
        [Route("~/api/puttempoproducao/{tempo}")]
        [HttpPut]
        public IActionResult UpdateTempo(Guid id, string tempo)
        {
            Produto p = repository.UpdateTempo(id, tempo);

            if (p == null)
            {
                return NotFound();
            }

            return Ok(p);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            Produto p = repository.Delete(id);

            if (p == null)
            {
                return NotFound();
            }
            return Ok();
        }

    }
}