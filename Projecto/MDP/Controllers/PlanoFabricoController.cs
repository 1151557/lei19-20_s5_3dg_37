using Microsoft.AspNetCore.Mvc;
using MDP.Model;
using MDP.Factories;
using MDP.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using MDF.Model;

namespace MDP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlanoFabricoController : ControllerBase
    {
        private readonly MDPContext _context;

        private PlanoFabricoFactory factory;

        private PlanoFabricoOperacaoFactory factoryitm;

        private PlanoFabricoRepository repository;

        private PlanoFabricoOperacaoRepository repopfo;

        public PlanoFabricoController(MDPContext context)
        {
            _context = context;
            factory = new PlanoFabricoFactory(context);
            factoryitm = new PlanoFabricoOperacaoFactory(context);
            repository = new PlanoFabricoRepository(context);
            repopfo = new PlanoFabricoOperacaoRepository(context);
        }

        [Route("~/api/GetAllPlanosFabrico")]
        [HttpGet(Name = "GetAllPlanosFabrico")]

        public IQueryable<PlanoFabrico> GetAll()
        {

            List<PlanoFabrico> listProdutos = repository.GetAll().ToList();

            return listProdutos.AsQueryable();
        }

        [HttpGet("{id}", Name = "GetPlanoFabrico")]
        public ActionResult<PlanoFabrico> GetById(Guid id)
        {
            PlanoFabrico pf = repository.GetById(id);
            if (pf == null)
            {
                return NotFound();
            }

            return pf;
        }


        [HttpPost]
        public IActionResult Create(PlanoFabricoDTO planoFabrico)
        {
            ICollection<Operacao> ops = repopfo.getOperacoes(planoFabrico.Operacoes).Result;
            PlanoFabrico pf = factory.newPlanoFabrico(planoFabrico);
            List<PlanoFabricoOperacao> pfo = factoryitm.newPlanoFabricoOperacao(pf, planoFabrico, ops);
            pf.PlanoFabricoOperacao = pfo;
            repository.Create(pf);
            return CreatedAtRoute("GetPlanoFabrico", new { id = pf.Id }, pf);
        }

        [HttpPut("{id}")]
        public IActionResult Update(Guid id, PlanoFabricoDTO planoFabrico)
        {
            ICollection<Operacao> ops = repopfo.getOperacoes(planoFabrico.Operacoes).Result;
            PlanoFabrico pf = repository.GetById(id);
            List<PlanoFabricoOperacao> pfo = factoryitm.newPlanoFabricoOperacao(pf, planoFabrico, ops);

            pf.PlanoFabricoOperacao = pfo;
            pf = repository.Update(id, pf);

            if (pf == null)
            {
                return NotFound();
            }
            return Ok(pf);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            PlanoFabrico pf = repository.Delete(id);

            if (pf == null)
            {
                return NotFound();
            }
            return Ok();
        }
    }
}