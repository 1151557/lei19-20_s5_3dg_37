using System;
using MDF.Model;
using MDP.Model;
using System.Collections.Generic;
using System.Linq;
namespace MDP.Factories
{

    class PlanoFabricoOperacaoFactory
    {
        private readonly MDPContext _context;

        public PlanoFabricoOperacaoFactory(MDPContext context)
        {
            _context = context;
        }

        public List<PlanoFabricoOperacao> newPlanoFabricoOperacao(PlanoFabrico tm, PlanoFabricoDTO pfdto, ICollection<Operacao> ops)
        {
            List<PlanoFabricoOperacao> ret = new List<PlanoFabricoOperacao>();
            if (pfdto.Operacoes != null)
            {
                foreach (Operacao abanador in ops)
                {
                    PlanoFabricoOperacao pfo = new PlanoFabricoOperacao();
                    pfo.PlanoFabricoId = tm.Id;
                    pfo.PlanoFabrico = tm;
                    pfo.Operacao = abanador;
                    pfo.OperacaoId = abanador.Id;
                    ret.Add(pfo);
                }
            }
            return ret;
        }
    }
}