using System;
using MDP.Model;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace MDP.Factories
{
    class ProdutoFactory
    {
        private readonly MDPContext _context;

        public ProdutoFactory(MDPContext context)
        {
            _context = context;
        }

        public Produto newProduto(ProdutoDTO pDTO)
        {
            Produto p = new Produto();
            p.Nome = pDTO.Nome;
            Guid guid = new Guid(pDTO.PFabrico);
            PlanoFabrico pfabrico = _context.PlanosFabrico.Single(pf => pf.Id == guid);
            p.Id = Guid.NewGuid();
            p.PFabrico = pfabrico;
            p.TempoProducao = pDTO.TempoProducao;
            return p;
        }

        public PlanoFabrico getProductoPlanoFabrico(Produto p)
        {
            if (p == null)
            {
                return null;
            }
            PlanoFabrico pfabrico = _context.PlanosFabrico.Include(psf => psf.PlanoFabricoOperacao).Single(pf => pf.Id == p.PFabrico.Id);
            return pfabrico;
        }
    }

    
}