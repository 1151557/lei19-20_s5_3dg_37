using System;
using MDP.Model;
using System.Collections.Generic;

namespace MDP.Factories
{
    class PlanoFabricoFactory
    {
        private readonly MDPContext _context;

        public PlanoFabricoFactory(MDPContext context)
        {
            _context = context;
        }

        public PlanoFabrico newPlanoFabrico(PlanoFabricoDTO pfDTO)
        {
            PlanoFabrico pf = new PlanoFabrico();
            pf.Id = Guid.NewGuid();
            pf.Nome = pfDTO.Nome;
            pf.PlanoFabricoOperacao = new List<PlanoFabricoOperacao>();
            return pf;
        }
    }
}