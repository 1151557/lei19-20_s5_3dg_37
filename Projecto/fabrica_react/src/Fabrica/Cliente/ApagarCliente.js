import React, { Component } from 'react'
import { connect } from 'react-redux'
import axios from 'axios'
import { BrowserRouter, Redirect } from 'react-router-dom'
import { signOut } from '../../store/actions/login/authActions'

class ApagarCliente extends Component {
    constructor(props) {
        super(props);
    }
    handleClick = (e) => {
        e.preventDefault();
        if (window.confirm("Deseja realmente apagar a sua conta? Este ato é irreversível")) {
            axios.delete('http://localhost:3001/encomendas/deletebycliente/' + localStorage.getItem('cli_id'))
            axios.delete('http://10.9.10.37:3001/clientes/' + localStorage.getItem('cli_id')).then(this.props.signOut)
            this.render()
        }
        else {
            window.alert("Não foi possível apagar a conta.")
        }
    }

    render() {
        if (localStorage.getItem('logged') != 'true') return (
            <BrowserRouter>
                <div>
                    <Redirect to='/signin' />
                </div>
            </BrowserRouter>
        );
        return (
            <div className="center">
                <br />
                <span className="input-group-btn">
                    <button className="btn pink lighten-1 z-depth-0" onClick={this.handleClick}>Apagar conta</button>
                </span>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        signOut: () => dispatch(signOut())
    }
}

export default connect(null, mapDispatchToProps)(ApagarCliente)