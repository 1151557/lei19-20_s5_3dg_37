# Projeto de ARQSI 2019 do Grupo [3DG-37]#

# 1. Constituição do Grupo de Trabalho ###

O grupo de trabalho é constituído pelo estudantes identificados na tabela seguinte.

| Aluno Nr.	   | Nome do Aluno			    |
|--------------|------------------------------|
| **1161777**  | Ana Inês Dos Santos Costa               |
| **1151557**  | Élio António Pinto Machado              |
| **1161641**  | Miguel Aureliano Barradas Costa         |
| **1171249**  | Renato Rafael Lopes Oliveira            |

# 2. Sprints ###

As pastas correspondentes a cada Sprint contem os respetivos diagramas de sequencia, de componentes e de conteúdo.

* [**Sprint-A**](/Documentacao/SPRINT-A/)

* [**Sprint-B**](/Documentacao/SPRINT-B/)

* [**Sprint-C**](/Documentacao/SPRINT-C/)

* [**Sprint-D**](/Documentacao/SPRINT-D/)

# 3. Modelo Domínio ###

![](Documentacao/Modelo_Dominio/ModeloDominio.jpg)
